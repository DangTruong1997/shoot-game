"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sortByProperty = exports.errorHandlertask = exports.compareTimeWithNow = exports.getCurrentTime = exports.formatTime = exports.copyObjectValue = exports.getRandomArrElement = exports.sendToClient = exports.BaseResponse = void 0;
class BaseResponse {
    constructor(code, data) {
        this.code = code || 400;
        this.data = data || "";
    }
}
exports.BaseResponse = BaseResponse;
const sendToClient = (res, baseResponse) => {
    return res.status(baseResponse.code).send(baseResponse.data);
};
exports.sendToClient = sendToClient;
const getRandomArrElement = (arr) => arr[Math.floor(Math.random() * arr.length)];
exports.getRandomArrElement = getRandomArrElement;
const copyObjectValue = (obj) => {
    let copyvalue = JSON.parse(JSON.stringify(obj));
    return copyvalue;
};
exports.copyObjectValue = copyObjectValue;
const formatTime = (millTime) => {
    let minute = Math.floor(millTime / 60000);
    let second = Math.floor(millTime / 1000) - (minute * 60);
    minute = minute < 10 ? `0${minute}` : minute;
    second = second < 10 ? `0${second}` : second;
    return `${minute}:${second}`;
};
exports.formatTime = formatTime;
const getCurrentTime = () => {
    return new Date().getTime();
};
exports.getCurrentTime = getCurrentTime;
const compareTimeWithNow = (time) => {
    return exports.getCurrentTime() - new Date(time).getTime();
};
exports.compareTimeWithNow = compareTimeWithNow;
const errorHandlertask = (task, errHandler) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield task();
    }
    catch (error) {
        console.log(error);
        if (errHandler) {
            yield errHandler();
        }
    }
});
exports.errorHandlertask = errorHandlertask;
const sortByProperty = (arrObj) => {
    return arrObj.sort((a, b) => (a.pos > b.pos) ? -1 : ((b.pos > a.pos) ? 1 : 0));
};
exports.sortByProperty = sortByProperty;
