"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkPlayerHeart = exports.updatePointAndRemoveGift = exports.checkHit = exports.giftAvailable = exports.addGifts = exports.addGiftWithoutValidation = exports.increaseTurn = exports.decreaseTurn = exports.enoughTurn = exports.listGift = exports.baseGameConfig = exports.initClientData = void 0;
const utils_1 = require("./utils");
globalThis.game = {};
globalThis.userInfo = {};
exports.initClientData = {
    "sprite": {
        "menu": {
            "srcImgBG": "http://cdn-dev.ugame.vn/srcpen/Sprite/1x/Covid-05.png",
            "srcImgLogo": "http://cdn-dev.ugame.vn/srcpen/Sprite/1x/Asset%203.png",
            "srcImgBtnPlay": "http://cdn-dev.ugame.vn/srcpen/Sprite/1x/Asset%204.png",
            "srcImgBtnTheLe": "http://cdn-dev.ugame.vn/srcpen/Sprite/1x/Asset%201.png",
            "srcImgBtnPhanThuong": "http://cdn-dev.ugame.vn/srcpen/Sprite/1x/Asset%202.png"
        },
        "game": {
            "srcImgBG": "http://cdn-dev.ugame.vn/srcpen/Sprite/1x/Covid-05.png",
            "srcImgHeart": "http://cdn-dev.ugame.vn/srcpen/Sprite/1x/Asset%209.png",
            "srcImgAvatar": "http://cdn-dev.ugame.vn/srcpen/Sprite/56942607_1112673272252173_1378655927598579712_o.jpg",
            "srcImgBtnPause": "http://cdn-dev.ugame.vn/srcpen/Sprite/1x/Asset%2010.png",
            "srcImgBtnPlayInPopupPause": "http://cdn-dev.ugame.vn/srcpen/Sprite/1x/Asset%204.png",
            "srcImgBGInPopupGameOver1": "http://cdn-dev.ugame.vn/srcpen/Sprite/OBJ/Asset%2031.png",
            "srcImgBtnPlayAgainInPopupGameOver1": "http://cdn-dev.ugame.vn/srcpen/Sprite/OBJ/Asset%2033.png",
            "srcImgLogoInPopupGameOver1": "http://cdn-dev.ugame.vn/srcpen/Sprite/OBJ/Asset%2032.png",
            "srcImgBGInPopupGameOver2": "http://cdn-dev.ugame.vn/srcpen/Sprite/OBJ/Asset%2031.png",
            "srcImgBtnCloseAgainInPopupGameOver2": "http://cdn-dev.ugame.vn/srcpen/Sprite/1x/Asset%2015.png",
            "srcimgLogoInPopupGameOver2": "http://cdn-dev.ugame.vn/srcpen/Sprite/OBJ/Asset%2032.png",
            "srcImgBgPopupMenu": "http://cdn-dev.ugame.vn/srcpen/Sprite/OBJ/Asset%2031.png",
            "srcImgClosePopupMenu": "http://cdn-dev.ugame.vn/srcpen/Sprite/X%20copy.png"
        }
    },
    "parameter_game": {
        "countCreateNewAnim": 5,
        "cycleMin": 0.5,
        "cycleMax": 3,
        "deltaCycle": 0.5,
        "deltaTimeCycle": 5,
        "ratioTwoGift": 0,
        "listItem": [
            {
                "typeGift": 0,
                "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/32323.png",
                "score": 10,
                "heart": 0,
                "ratio": 5
            },
            {
                "typeGift": 0,
                "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/242424.png",
                "score": 10,
                "heart": 0,
                "ratio": 5
            },
            {
                "typeGift": 0,
                "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/353635.png",
                "score": 10,
                "heart": 0,
                "ratio": 10
            },
            {
                "typeGift": 0,
                "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/646453.png",
                "score": 10,
                "heart": 0,
                "ratio": 10
            },
            {
                "typeGift": 0,
                "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/53242245.png",
                "score": 10,
                "heart": 0,
                "ratio": 10
            },
            {
                "typeGift": 0,
                "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/5342252565.png",
                "score": 10,
                "heart": 0,
                "ratio": 10
            },
            {
                "typeGift": 1,
                "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/Asset%2024.png",
                "score": 0,
                "heart": -1,
                "ratio": 10
            },
            {
                "typeGift": 2,
                "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/Asset%2025.png",
                "score": 0,
                "heart": 1,
                "ratio": 10
            },
            {
                "typeGift": 0,
                "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/Asset%2027.png",
                "score": 10,
                "heart": 0,
                "ratio": 10
            },
            {
                "typeGift": 0,
                "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/Asset%2028.png",
                "score": 10,
                "heart": 0,
                "ratio": 10
            },
            {
                "typeGift": 0,
                "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/Asset%2029.png",
                "score": 10,
                "heart": 0,
                "ratio": 10
            }
        ]
    },
    "audio": {
        "srcAudioBG": "http://cdn-dev.ugame.vn/srcpen/music/1.%20Bgm.mp3",
        "srcAudioTrue": "http://cdn-dev.ugame.vn/srcpen/music/5.%20Success.mp3",
        "srcAudioFalse": "http://cdn-dev.ugame.vn/srcpen/music/6.%20Unsuccess.mp3"
    },
    "config_string": {
        "countCreateNewAnim": 2,
        "cycleMin": 1.5,
        "cycleMax": 3,
        "deltaCycle": 0.3,
        "deltaTimeCycle": 15,
        "ratioTwoGift": 100,
        "countPlay": 1000,
        "url": "https://27db95c19f06.ngrok.io/"
    },
    "config_content_popup": {
        "strBeforeCountPlay": "BẠN CÒN ",
        "strAfterCountPlay": " LƯỢT CHƠI",
        "strTitleTheLe": "THỂ LỆ",
        "strTitlePhanThuong": "PHẦN THƯỞNG",
        "strTheLe": "Cựu võ sĩ người Mỹ gốc Việt Nam, Cung Lê từng là một nhà vô địch thế giới Kickboxing và từng là tay đấm xuất sắc của UFC. Sau khi giải nghệ anh chuyển sang nghiệp diễn xuất, tuy nhiên người đàn ông 48 tuổi vẫn tập luyện chăm chỉ mỗi ngày, do đó Cung Lê vẫn giữ được cơ thể tráng kiện chẳng kém hồi còn thi đấu là bao. ",
        "strPhanThuong": "Cựu võ sĩ người Mỹ gốc Việt Nam, Cung Lê từng là một nhà vô địch thế giới Kickboxing và từng là tay đấm xuất sắc của UFC. Sau khi giải nghệ anh chuyển sang nghiệp diễn xuất, tuy nhiên người đàn ông 48 tuổi vẫn tập luyện chăm chỉ mỗi ngày, do đó Cung Lê vẫn giữ được cơ thể tráng kiện chẳng kém hồi còn thi đấu là bao. "
    },
    "hashtag": {
        "rule": "#popup_rule",
        "gift": "#popup_gift",
        "pause": "#pause",
        "resume": "#resume",
        "gameover1": "#gameover1",
        "gameover2": "#gameover2"
    },
    "test_game": {
        "login": 1
    }
};
exports.baseGameConfig = {
    gifts: [],
    heart: 3,
    point: 0,
    lastHeartBeat: null,
    start_at: null
};
exports.listGift = [
    {
        "name": "beer1",
        "typeGift": 0,
        "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/32323.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "beer2",
        "typeGift": 0,
        "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/242424.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "beer3",
        "typeGift": 0,
        "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/646453.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "gift1",
        "typeGift": 0,
        "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/353635.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "gift2",
        "typeGift": 0,
        "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/5342252565.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "milk1",
        "typeGift": 0,
        "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/53242245.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "boom1",
        "typeGift": 1,
        "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/Asset%2024.png",
        "score": 0,
        "heart": -1
    },
    {
        "name": "heart1",
        "typeGift": 2,
        "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/Asset%2025.png",
        "score": 0,
        "heart": 1
    },
    {
        "name": "virus1",
        "typeGift": 0,
        "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/Asset%2027.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "virus2",
        "typeGift": 0,
        "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/Asset%2028.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "virus3",
        "typeGift": 0,
        "srcImage": "http://cdn-dev.ugame.vn/srcpen/Sprite/Item/Asset%2029.png",
        "score": 10,
        "heart": 0
    }
];
const enoughTurn = (socketId) => {
    if (globalThis.userInfo[socketId].turnNumber >= 0) {
        return true;
    }
    return false;
};
exports.enoughTurn = enoughTurn;
const decreaseTurn = (socketId) => {
    globalThis.userInfo[socketId].turnNumber--;
};
exports.decreaseTurn = decreaseTurn;
const increaseTurn = (socketId) => {
    globalThis.userInfo[socketId].turnNumber++;
};
exports.increaseTurn = increaseTurn;
const generateGift = () => {
    const gift = utils_1.getRandomArrElement(exports.listGift);
    gift.id = Math.random().toString(36).substring(7);
    return gift;
};
const addGiftWithoutValidation = (socketId) => {
    console.log(socketId);
    globalThis.game[socketId].gifts = [generateGift(), generateGift()];
};
exports.addGiftWithoutValidation = addGiftWithoutValidation;
const addGifts = (socketId) => {
    if (!globalThis.game[socketId] || globalThis.game[socketId].gifts.length > 0) {
        return;
    }
    globalThis.game[socketId].gifts = [generateGift(), generateGift()];
};
exports.addGifts = addGifts;
const giftAvailable = (socketId, giftId) => {
    const obj = globalThis.game[socketId].gifts.filter(o => {
        return o.id == giftId;
    });
    if (obj.length > 0) {
        return true;
    }
    return false;
};
exports.giftAvailable = giftAvailable;
const checkHit = (pos, posX, cycle, colliderWidth, statusMoveLeft) => {
    if (pos == 0) {
        if (!statusMoveLeft) {
            if (Math.abs(posX - 900 / cycle * 0.26) < (colliderWidth / 2 + 15)) {
                return true;
            }
        }
        else {
            if (Math.abs(posX + 900 / cycle * 0.26) < (colliderWidth / 2 + 15)) {
                return true;
            }
        }
    }
    else {
        if (!statusMoveLeft) {
            if (Math.abs(posX - 900 / cycle * 0.385) < (colliderWidth / 2 + 15)) {
                return true;
            }
        }
        else {
            if (Math.abs(posX + 900 / cycle * 0.385) < (colliderWidth / 2 + 15)) {
                return true;
            }
        }
    }
    return false;
};
exports.checkHit = checkHit;
const updatePointAndRemoveGift = (socketId, giftId) => {
    const game = globalThis.game[socketId];
    const gift = globalThis.game[socketId].gifts.filter(gift => gift.id == giftId)[0];
    game.point += gift.score;
    game.heart += gift.heart;
    return game.gifts.splice(game.gifts.indexOf(gift), 1);
};
exports.updatePointAndRemoveGift = updatePointAndRemoveGift;
const checkPlayerHeart = (socketId) => {
    console.log('zero heart');
    if (globalThis.game[socketId].heart > 0) {
        return;
    }
    return delete globalThis.game[socketId];
};
exports.checkPlayerHeart = checkPlayerHeart;
const checkHeartBeat = () => {
    for (const socketId in globalThis.game) {
        if (globalThis.game[socketId] && utils_1.compareTimeWithNow(globalThis.game[socketId].lastHeartBeat) > 10000) {
            console.log(`Player ${socketId} has been disconnected!`);
            delete globalThis.game[socketId];
        }
    }
};
setInterval(checkHeartBeat, 1000);
