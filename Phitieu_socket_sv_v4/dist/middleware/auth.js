"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.auth = void 0;
const configs_1 = require("./../utils/configs");
const utils_1 = require("./../utils/utils");
const jwt = __importStar(require("jsonwebtoken"));
const auth = (req, res, next) => {
    const token = req.cookies.game_token;
    try {
        if (!token) {
            throw "Invalid token";
        }
        const decode = jwt.verify(token, configs_1.APP_CONFIG.JWT_SECRET);
        if (!decode.uId) {
            throw "Invalid token";
        }
        res.locals.user = decode;
        if (!req.session.player)
            req.session.player = decode;
        return next();
    }
    catch (error) {
        const baseResponse = new utils_1.BaseResponse(400, "Not auth");
        return utils_1.sendToClient(res, baseResponse);
    }
};
exports.auth = auth;
