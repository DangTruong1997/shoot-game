"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const gameUtils_1 = require("./utils/gameUtils");
const utils_1 = require("./utils/utils");
const socket_io_1 = require("socket.io");
class SOCKET {
    constructor(server) {
        this.io = new socket_io_1.Server(server);
        this.game = this.io.of('/shootgame');
        this.middlewares();
        this.config();
    }
    middlewares() {
        this.game.use((socket, next) => {
            if (!socket.handshake.query) {
                return next(new Error("Not auth"));
            }
            try {
                const { userId } = socket.handshake.query;
                if (!globalThis.userInfo[userId]) {
                    globalThis.userInfo[userId] = {
                        "turnNumber": 100,
                        "uName": "Test",
                        "uAvatar": "https://cdn.discordapp.com/emojis/582573374426054713.png?v=1",
                        "id": userId,
                    };
                }
                socket.id = userId;
                socket.join(socket.id);
                return next();
            }
            catch (error) {
                return next(new Error(error));
            }
        });
    }
    config() {
        this.game.on("connection", function (socket) {
            console.log(`Player ${socket.id} ONLINE`);
            socket.on('init-data', function (data, callback) {
                if (!callback) {
                    return;
                }
                const task = () => {
                    return callback(null, {
                        userData: globalThis.userInfo[socket.id],
                        gameConfig: gameUtils_1.initClientData
                    });
                };
                return utils_1.errorHandlertask(task);
            });
            socket.on('play', function (data, callback) {
                const task = () => {
                    if (!callback) {
                        return;
                    }
                    if (!gameUtils_1.enoughTurn(socket.id)) {
                        return callback("not-enough-turn", "User don't have enough turn to play.");
                    }
                    if (globalThis.game[socket.id]) {
                    }
                    globalThis.game[socket.id] = utils_1.copyObjectValue(gameUtils_1.baseGameConfig);
                    globalThis.game[socket.id].lastHeartBeat = utils_1.getCurrentTime();
                    globalThis.game[socket.id].start_at = utils_1.getCurrentTime();
                    gameUtils_1.addGifts(socket.id);
                    gameUtils_1.decreaseTurn(socket.id);
                    return callback(null, globalThis.game[socket.id]);
                };
                return utils_1.errorHandlertask(task);
            });
            socket.on("heartbeat", function () {
                const task = () => {
                    if (globalThis.game[socket.id]) {
                        globalThis.game[socket.id].lastHeartBeat = new Date().getTime();
                    }
                };
                return utils_1.errorHandlertask(task);
            });
            socket.on('throw-arrow', function (data, callback) {
                console.log("data:", data);
                const task = () => {
                    if (!data || !callback || !globalThis.game[socket.id]) {
                        return;
                    }
                    let legalHit = [];
                    for (let i = 0; i < data.length; i++) {
                        if (!gameUtils_1.giftAvailable(socket.id, data[i].id)) {
                            return callback("Gift not available.", null);
                        }
                        console.log("Is hit:", gameUtils_1.checkHit(data[i].pos, data[i].posX, data[i].cycle, data[i].collideWidth, data[i].statusMoveLeft));
                        if (gameUtils_1.checkHit(data[i].pos, data[i].posX, data[i].cycle, data[i].collideWidth, data[i].statusMoveLeft)) {
                            legalHit.push(data[i]);
                        }
                    }
                    if (legalHit.length == 0) {
                        globalThis.game[socket.id].heart--;
                        callback(null, { hit: null, data: globalThis.game[socket.id] });
                        return gameUtils_1.checkPlayerHeart(socket.id);
                    }
                    console.log("console.log legalHit:", legalHit);
                    gameUtils_1.updatePointAndRemoveGift(socket.id, legalHit[0].id);
                    return callback(null, { hit: legalHit[0], data: globalThis.game[socket.id] });
                };
                return utils_1.errorHandlertask(task);
            });
            socket.on('pause-game', function () {
                console.log("PAUSE GAME");
            });
            socket.on('resume-game', function () {
                console.log("RESUME GAME");
            });
            socket.on('out-game', function () {
                delete globalThis.game[socket.id];
            });
            socket.on('receive-gift', function (data, callback) {
                const task = () => {
                    const gift = utils_1.getRandomArrElement(gameUtils_1.listGift);
                    return callback(null, gift);
                };
                return utils_1.errorHandlertask(task);
            });
            socket.on("disconnect", function () {
                console.log(`Player ${socket.id} temporarily disconnected!`);
            });
            socket.on('generate-gift', function (data, callback) {
                if (!callback) {
                    return;
                }
                const task = () => {
                    gameUtils_1.addGiftWithoutValidation(socket.id);
                    return callback('', globalThis.game[socket.id].gifts);
                };
                return utils_1.errorHandlertask(task);
            });
            socket.on('throw-arrow-2', function (data, callback) {
            });
        });
    }
}
exports.default = SOCKET;
