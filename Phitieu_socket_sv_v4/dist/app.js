"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.socket = void 0;
const configs_1 = require("./utils/configs");
const utils_1 = require("./utils/utils");
const http = __importStar(require("http"));
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const helmet_1 = __importDefault(require("helmet"));
const body_parser_1 = __importDefault(require("body-parser"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
const express_session_1 = __importDefault(require("express-session"));
const socket_1 = __importDefault(require("./socket"));
class App {
    constructor() {
        this.app = express_1.default();
        this.config();
    }
    config() {
        this.app.use(helmet_1.default());
        this.app.use(cors_1.default());
        this.app.use(body_parser_1.default.urlencoded({ extended: true }));
        this.app.use(body_parser_1.default.json());
        this.app.use(cookie_parser_1.default(configs_1.APP_CONFIG.COOKIE_SECRET));
        this.app.use(express_session_1.default({
            secret: configs_1.APP_CONFIG.SESSION_SECRET,
            resave: true,
            saveUninitialized: true,
        }));
        this.app.get("*", function (req, res) {
            return res.send("404 not found");
        });
        this.app.use((err, req, res, next) => {
            console.log(err);
            let baseResponse = new utils_1.BaseResponse(err.code || 500, err.message || "Đã có lỗi xảy ra, xin vui lòng thử lại sau.");
            return req.method == "GET" ? res.send("404 not found") : res.status(baseResponse.code).send(baseResponse);
        });
    }
}
let server = new http.Server(new App().app);
const socket = new socket_1.default(server);
exports.socket = socket;
exports.default = server;
