"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const configs_1 = require("./../utils/configs");
const utils_1 = require("./../utils/utils");
const jwt = __importStar(require("jsonwebtoken"));
class APIController {
    constructor() {
        this.baseResponse = new utils_1.BaseResponse(200);
        this.login = (req, res, next) => {
            this.baseResponse.data = {
                "turnNumber": 2,
                "uName": "Test",
                "uAvatar": "https://cdn.discordapp.com/emojis/582573374426054713.png?v=1",
                "id": Math.floor(Math.random() * 10001),
            };
            this.baseResponse.data.token = jwt.sign(this.baseResponse.data, configs_1.APP_CONFIG.JWT_SECRET, { expiresIn: '3d' });
            return utils_1.sendToClient(res, this.baseResponse);
        };
        this.getTurn = (req, res, next) => {
            const userId = req.query.userId.toString();
            if (!userId) {
                this.baseResponse.code = 500;
                this.baseResponse.data = "";
            }
            else {
                this.baseResponse.data = {
                    "turnNumber": globalThis[userId] ? globalThis[userId].turnNumber : 2,
                };
            }
            return utils_1.sendToClient(res, this.baseResponse);
        };
    }
}
exports.default = APIController;
