import { APP_CONFIG } from './../utils/configs';
import { BaseResponse, sendToClient } from './../utils/utils';
import { Request, Response, NextFunction } from 'express';
import * as jwt from "jsonwebtoken";

export const auth = (req: Request, res: Response, next: NextFunction) => {
    const token = req.cookies.game_token;
    try {
        if (!token) {
            throw "Invalid token";
        }
        const decode: any = jwt.verify(token, APP_CONFIG.JWT_SECRET);
        if (!decode.uId) {
            throw "Invalid token";
        }
        res.locals.user = decode;
        if (!req.session.player)
            req.session.player = decode;
        return next();
    } catch (error) {
        const baseResponse = new BaseResponse(400, "Not auth");
        return sendToClient(res, baseResponse);
    }
}