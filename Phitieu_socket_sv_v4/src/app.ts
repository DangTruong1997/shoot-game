import { APP_CONFIG } from './utils/configs';
import { BaseResponse } from './utils/utils';
import * as http from "http";
import express from "express";
import cors from "cors";
import helmet from "helmet";
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import SOCKET from "./socket";
// import routes from './route/index';

class App {
    public app: express.Application;
    constructor() {
        this.app = express();
        this.config();
    }

    private config(): void {
        // Config middlewares
        this.app.use(helmet());
        this.app.use(cors());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        this.app.use(cookieParser(APP_CONFIG.COOKIE_SECRET));
        this.app.use(
            session({
                secret: APP_CONFIG.SESSION_SECRET,
                resave: true,
                saveUninitialized: true,
            })
        );
        // Using routes
        // this.app.use(routes);
        this.app.get("*", function (req, res) {
            return res.send("404 not found");
        });
        //Error handing
        this.app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
            console.log(err);
            let baseResponse = new BaseResponse(err.code || 500, err.message || "Đã có lỗi xảy ra, xin vui lòng thử lại sau.");
            return req.method == "GET" ? res.send("404 not found") : res.status(baseResponse.code).send(baseResponse);
        });
    }
}

let server = new http.Server(new App().app);
const socket = new SOCKET(server);

export default server;
export { socket };
