import { baseGameConfig, listGift, enoughTurn, decreaseTurn, addGifts, addGiftWithoutValidation, giftAvailable, checkHit, checkPlayerHeart, updatePointAndRemoveGift, initClientData } from './utils/gameUtils';
import * as http from "http";
import { getRandomArrElement, copyObjectValue, getCurrentTime, errorHandlertask, sortByProperty } from './utils/utils';
import { Namespace, Server, Socket } from 'socket.io';

class SOCKET {
  public io: Server;
  private game: Namespace
  constructor(server: http.Server) {
    this.io = new Server(server);
    this.game = this.io.of('/shootgame');
    this.middlewares();
    this.config();
  }

  private middlewares() {

    //thông tin người chơi
    this.game.use((socket, next) => {
      if (!socket.handshake.query) {
        return next(new Error("Not auth"));
      }
      try {
        const { userId }: any = socket.handshake.query;
        if (!globalThis.userInfo[userId]) {
          globalThis.userInfo[userId] = {
            "turnNumber": 100,
            "uName": "Test",
            "uAvatar": "https://cdn.discordapp.com/emojis/582573374426054713.png?v=1",
            "id": userId,
          };
        }
        socket.id = userId;
        socket.join(socket.id);
        return next();
      } catch (error) {
        return next(new Error(error));
      }
    });
  }

  private config(): void {

    this.game.on("connection", function (socket: Socket) {
      console.log(`Player ${socket.id} ONLINE`);

      //thông tin game : thể lệ ...
      socket.on('init-data', function (data, callback) {
        if (!callback) {
          return;
        }
        const task = () => {
          return callback(null, {
            userData: globalThis.userInfo[socket.id],
            gameConfig: initClientData
          })
        }
        return errorHandlertask(task);
      });

      //chơi game
      socket.on('play', function (data, callback) {
        const task = () => {
          if (!callback) {
            return;
          }

          if (!enoughTurn(socket.id)) {
            return callback("not-enough-turn", "User don't have enough turn to play.");
          }

          if (globalThis.game[socket.id]) {
            //Save result.
            // return callback("play-failed", "Can not start new game when playing a game.");
          }

          globalThis.game[socket.id] = copyObjectValue(baseGameConfig);
          globalThis.game[socket.id].lastHeartBeat = getCurrentTime();
          globalThis.game[socket.id].start_at = getCurrentTime();
          addGifts(socket.id);
          decreaseTurn(socket.id);
          return callback(null, globalThis.game[socket.id]);
        }
        return errorHandlertask(task);
      });

      //ping check đứt kết nối
      socket.on("heartbeat", function () {
        const task = () => {
          if (globalThis.game[socket.id]) {
            globalThis.game[socket.id].lastHeartBeat = new Date().getTime();
          }
        }
        return errorHandlertask(task);
      });

      //phi tiêu
      socket.on('throw-arrow', function (data, callback) {
        console.log("data:", data);
        const task = () => {
          if (!data || !callback || !globalThis.game[socket.id]) {
            return;
          }
          let legalHit = [];
          for (let i = 0; i < data.length; i++) {
            if (!giftAvailable(socket.id, data[i].id)) {
              return callback("Gift not available.", null);
            }
            console.log("Is hit:", checkHit(data[i].pos, data[i].posX, data[i].cycle, data[i].collideWidth, data[i].statusMoveLeft));
            if (checkHit(data[i].pos, data[i].posX, data[i].cycle, data[i].collideWidth, data[i].statusMoveLeft)) {
              legalHit.push(data[i]);
            }
          }
          if (legalHit.length == 0) {
            globalThis.game[socket.id].heart--;
            callback(null, { hit: null, data: globalThis.game[socket.id] });
            return checkPlayerHeart(socket.id);
          }
          console.log("console.log legalHit:", legalHit);
          updatePointAndRemoveGift(socket.id, legalHit[0].id);
          return callback(null, { hit: legalHit[0], data: globalThis.game[socket.id] });
        }
        return errorHandlertask(task);
      });


      socket.on('pause-game', function () {
        console.log("PAUSE GAME");
      });

      socket.on('resume-game', function () {
        console.log("RESUME GAME");
      });

      socket.on('out-game', function () {
        // Update result
        delete globalThis.game[socket.id];
      });

      //nhận quà
      socket.on('receive-gift', function (data, callback) {
        console.log("receive-gift");
        const task = () => {
          // Return gift
          const gift = getRandomArrElement(listGift);
          return callback(null, gift);
        }
        return errorHandlertask(task);
      });

      socket.on("disconnect", function () {
        console.log(`Player ${socket.id} temporarily disconnected!`);
      });

      //sinh quà
      socket.on('generate-gift', function (data, callback) {
        if (!callback) {
          return;
        }
        const task = () => {
          addGiftWithoutValidation(socket.id);
          return callback('', globalThis.game[socket.id].gifts);
        };
        return errorHandlertask(task);
      })

      socket.on('throw-arrow-2', function (data, callback) {
        // if (!callback || !data) {
        //     return;
        // }
        // if (data == "miss") {
        //     globalThis.game[socket.id].heart--;
        //     checkPlayerHeart(socket.id);
        //     return callback(null, globalThis.game[socket.id]);
        // }
        // const task = () => {
        //     if (!giftAvailable(socket.id, data.id)) {
        //         return callback("Gift not available.", null);
        //     }
        //     updatePointAndRemoveGift(socket.id, data.id);
        //     return callback(null, globalThis.game[socket.id]);
        // }
        // return errorHandlertask(task);
      });

    });
  }

}

export default SOCKET;