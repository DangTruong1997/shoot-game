import './utils/gameUtils'
import { APP_CONFIG } from './utils/configs';
import server from "./app";

const port = process.env.PORT || APP_CONFIG.PORT;
server.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});