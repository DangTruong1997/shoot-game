import { getRandomArrElement, compareTimeWithNow } from './utils';

globalThis.game = {};

globalThis.userInfo = {};

export const initClientData = {
    "sprite": {
        "menu": {
            "srcImgBG": "https://botplus.io/shootgame/Sprite/1x/Covid-05.png",
            "srcImgLogo": "https://botplus.io/shootgame/Sprite/1x/Asset%203.png",
            "srcImgBtnPlay": "https://botplus.io/shootgame/Sprite/1x/Asset%204.png",
            "srcImgBtnTheLe": "https://botplus.io/shootgame/Sprite/1x/Asset%201.png",
            "srcImgBtnPhanThuong": "https://botplus.io/shootgame/Sprite/1x/Asset%202.png"
        },
        "game": {
            "srcImgBG": "https://botplus.io/shootgame/Sprite/1x/Covid-05.png",
            "srcImgHeart": "https://botplus.io/shootgame/Sprite/1x/Asset%209.png", //trái tim
            "srcImgAvatar": "https://botplus.io/shootgame/Sprite/56942607_1112673272252173_1378655927598579712_o.jpg",
            "srcImgBtnPause": "https://botplus.io/shootgame/Sprite/1x/Asset%2010.png", //pause
            "srcImgBtnPlayInPopupPause": "https://botplus.io/shootgame/Sprite/1x/Asset%204.png",
            "srcImgBGInPopupGameOver1": "https://botplus.io/shootgame/Sprite/OBJ/Asset%2031.png", //logo gameover 1
            "srcImgBtnPlayAgainInPopupGameOver1": "https://botplus.io/shootgame/Sprite/OBJ/Asset%2033.png", //nút playagain game over 1
            "srcImgLogoInPopupGameOver1": "https://botplus.io/shootgame/Sprite/OBJ/Asset%2032.png",
            "srcImgBGInPopupGameOver2": "https://botplus.io/shootgame/Sprite/OBJ/Asset%2031.png", 
            "srcImgBtnCloseAgainInPopupGameOver2": "https://botplus.io/shootgame/Sprite/1x/Asset%2015.png",//nút close game over 2
            "srcimgLogoInPopupGameOver2": "https://botplus.io/shootgame/Sprite/OBJ/Asset%2032.png",//logo gameover 2
            "srcImgBgPopupMenu":"https://botplus.io/shootgame/Sprite/OBJ/Asset%2031.png",
            "srcImgClosePopupMenu":"https://botplus.io/shootgame/Sprite/X%20copy.png",
            "srcImgArrow":"https://botplus.io/shootgame/Sprite/X%20copy.png"//mũi tên
        }
    },
    "parameter_game": {
        // "countCreateNewAnim": 5,//sau bao nhiêu lần va chạm vào thành sẽ đổi item mới
        // "cycleMin": 0.5,//tốc độ thấp nhất
        // "cycleMax": 3,//tốc độ nhanh nhất
        // "deltaCycle": 0.5,//tốc độ tăng trong mỗi lần : vd 5 giây tốc độ của item sẽ tăng thêm 0,5
        // "deltaTimeCycle": 5,//thời gian tăng tốc độ : vd 5 giây tăng tốc độ 1 lần
        // "ratioTwoGift": 0,//tỉ lệ 100% ra 2 quà
        "listItem": [
            {
                "typeGift": 0,//loại item thường
                "srcImage": "https://botplus.io/shootgame/Sprite/Item/32323.png",//hình ảnh
                "score": 10,//điểm cộng
                "heart": 0,//tim cộng
                "ratio": 5//tỉ lệ
            },
            {
                "typeGift": 0,
                "srcImage": "https://botplus.io/shootgame/Sprite/Item/242424.png",
                "score": 10,
                "heart": 0,
                "ratio": 5
            },
            {
                "typeGift": 0,
                "srcImage": "https://botplus.io/shootgame/Sprite/Item/353635.png",
                "score": 10,
                "heart": 0,
                "ratio": 10
            },
            {
                "typeGift": 0,
                "srcImage": "https://botplus.io/shootgame/Sprite/Item/646453.png",
                "score": 10,
                "heart": 0,
                "ratio": 10
            },
            {
                "typeGift": 0,
                "srcImage": "https://botplus.io/shootgame/Sprite/Item/53242245.png",
                "score": 10,
                "heart": 0,
                "ratio": 10
            },
            {
                "typeGift": 0,
                "srcImage": "https://botplus.io/shootgame/Sprite/Item/5342252565.png",
                "score": 10,
                "heart": 0,
                "ratio": 10
            },
            {
                "typeGift": 1,
                "srcImage": "https://botplus.io/shootgame/Sprite/Item/Asset%2024.png",
                "score": 0,
                "heart": -1,
                "ratio": 10
            },
            {
                "typeGift": 2,
                "srcImage": "https://botplus.io/shootgame/Sprite/Item/Asset%2025.png",
                "score": 0,
                "heart": 1,
                "ratio": 10
            },
            {
                "typeGift": 0,
                "srcImage": "https://botplus.io/shootgame/Sprite/Item/Asset%2027.png",
                "score": 10,
                "heart": 0,
                "ratio": 10
            },
            {
                "typeGift": 0,
                "srcImage": "https://botplus.io/shootgame/Sprite/Item/Asset%2028.png",
                "score": 10,
                "heart": 0,
                "ratio": 10
            },
            {
                "typeGift": 0,
                "srcImage": "https://botplus.io/shootgame/Sprite/Item/Asset%2029.png",
                "score": 10,
                "heart": 0,
                "ratio": 10
            }
        ]//danh sách item
        //typegift = 0 là loại thường
        //typegift = 1 là loại trừ tim
        //typegift = 2 là loại cộng tim
        //chú ý tổng ratio phải bằng 100 vì đây là tỉ lệ phát item
    },
    "audio": {
        "srcAudioBG": "https://botplus.io/shootgame/music/1.%20Bgm.mp3",
        "srcAudioTrue": "https://botplus.io/shootgame/music/5.%20Success.mp3",
        "srcAudioFalse": "https://botplus.io/shootgame/music/6.%20Unsuccess.mp3"
    },
    "config_string": {
        "countCreateNewAnim": 2,
        "cycleMin": 1.5,
        "cycleMax": 3,
        "deltaCycle": 0.3,
        "deltaTimeCycle": 15,
        "ratioTwoGift": 100,
        "countPlay": 1000,//số lượt chơi
        "url": "https://27db95c19f06.ngrok.io/"//avartar
    },
    "config_content_popup": {
        "strBeforeCountPlay": "BẠN CÒN ",
        "strAfterCountPlay": " LƯỢT CHƠI",
        "strTitleTheLe": "THỂ LỆ",
        "strKeyScore": "Điểm",
        "strKeytime": "Thời gian",
        "strTitlePhanThuong": "PHẦN THƯỞNG",
        "strTheLe": "Cựu võ sĩ người Mỹ gốc Việt Nam, Cung Lê từng là một nhà vô địch thế giới Kickboxing và từng là tay đấm xuất sắc của UFC. Sau khi giải nghệ anh chuyển sang nghiệp diễn xuất, tuy nhiên người đàn ông 48 tuổi vẫn tập luyện chăm chỉ mỗi ngày, do đó Cung Lê vẫn giữ được cơ thể tráng kiện chẳng kém hồi còn thi đấu là bao. ",
        "strPhanThuong": "Cựu võ sĩ người Mỹ gốc Việt Nam, Cung Lê từng là một nhà vô địch thế giới Kickboxing và từng là tay đấm xuất sắc của UFC. Sau khi giải nghệ anh chuyển sang nghiệp diễn xuất, tuy nhiên người đàn ông 48 tuổi vẫn tập luyện chăm chỉ mỗi ngày, do đó Cung Lê vẫn giữ được cơ thể tráng kiện chẳng kém hồi còn thi đấu là bao. "
    },
    "hashtag": {
        "rule": "#popup_rule",
        "gift": "#popup_gift",
        "pause": "#pause",
        "resume": "#resume",
        "gameover1": "#gameover1",
        "gameover2": "#gameover2"
    },
    "test_game": {
        "login": 1
    },
    "color_text": {
        "score": "#F10808",
        "heart": "#F10808"
    }

}


export const baseGameConfig = {
    gifts: [],
    heart: 3,
    point: 0,
    lastHeartBeat: null,
    start_at: null
}

export const listGift = [
    {
        "name": "beer1",
        "typeGift": 0,
        "srcImage": "https://botplus.io/shootgame/Sprite/Item/32323.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "beer2",
        "typeGift": 0,
        "srcImage": "https://botplus.io/shootgame/Sprite/Item/242424.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "beer3",
        "typeGift": 0,
        "srcImage": "https://botplus.io/shootgame/Sprite/Item/646453.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "gift1",
        "typeGift": 0,
        "srcImage": "https://botplus.io/shootgame/Sprite/Item/353635.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "gift2",
        "typeGift": 0,
        "srcImage": "https://botplus.io/shootgame/Sprite/Item/5342252565.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "milk1",
        "typeGift": 0,
        "srcImage": "https://botplus.io/shootgame/Sprite/Item/53242245.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "boom1",
        "typeGift": 1,
        "srcImage": "https://botplus.io/shootgame/Sprite/Item/Asset%2024.png",
        "score": 0,
        "heart": -1
    },
    {
        "name": "heart1",
        "typeGift": 2,
        "srcImage": "https://botplus.io/shootgame/Sprite/Item/Asset%2025.png",
        "score": 0,
        "heart": 1
    },
    {
        "name": "virus1",
        "typeGift": 0,
        "srcImage": "https://botplus.io/shootgame/Sprite/Item/Asset%2027.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "virus2",
        "typeGift": 0,
        "srcImage": "https://botplus.io/shootgame/Sprite/Item/Asset%2028.png",
        "score": 10,
        "heart": 0
    },
    {
        "name": "virus3",
        "typeGift": 0,
        "srcImage": "https://botplus.io/shootgame/Sprite/Item/Asset%2029.png",
        "score": 10,
        "heart": 0
    }
]

export const enoughTurn = (socketId: string) => {
    if (globalThis.userInfo[socketId].turnNumber >= 0) {
        return true;
    }
    return false;
}

export const decreaseTurn = (socketId: string) => {
    globalThis.userInfo[socketId].turnNumber--;
}

export const increaseTurn = (socketId: string) => {
    globalThis.userInfo[socketId].turnNumber++;
}

const generateGift = () => {
    const gift = getRandomArrElement(listGift);
    gift.id = Math.random().toString(36).substring(7);
    return gift;
}

export const addGiftWithoutValidation = (socketId: string) => {
    console.log(socketId);
    globalThis.game[socketId].gifts = [generateGift(), generateGift()];
}

export const addGifts = (socketId: string) => {
    if (!globalThis.game[socketId] || globalThis.game[socketId].gifts.length > 0) {
        return;
    }
    globalThis.game[socketId].gifts = [generateGift(), generateGift()];
}

export const giftAvailable = (socketId: string, giftId: string) => {
    const obj = globalThis.game[socketId].gifts.filter(o => {
        return o.id == giftId;
    });
    if (obj.length > 0) {
        return true;
    }
    return false;
}

export const checkHit = (pos: string | number, posX: number, cycle: number, colliderWidth: number, statusMoveLeft: Boolean) => {
    if (pos == 0) {
        if (!statusMoveLeft) {
            if (Math.abs(posX - 900 / cycle * 0.26) < (colliderWidth / 2 + 15)) {
                return true;
            }
        }
        else {
            if (Math.abs(posX + 900 / cycle * 0.26) < (colliderWidth / 2 + 15)) {
                return true;
            }
        }

    } else {
        if (!statusMoveLeft) {
            if (Math.abs(posX - 900 / cycle * 0.385) < (colliderWidth / 2 + 15)) {
                return true;
            }
        }
        else {
            if (Math.abs(posX + 900 / cycle * 0.385) < (colliderWidth / 2 + 15)) {
                return true;
            }
        }

    }
    return false;
}

export const updatePointAndRemoveGift = (socketId: string, giftId: string) => {
    const game = globalThis.game[socketId];
    const gift = globalThis.game[socketId].gifts.filter(gift => gift.id == giftId)[0];
    game.point += gift.score;
    game.heart += gift.heart;
    return game.gifts.splice(game.gifts.indexOf(gift), 1);
}

export const checkPlayerHeart = (socketId: string) => {
    console.log('zero heart');
    if (globalThis.game[socketId].heart > 0) {
        return;
    }
    return delete globalThis.game[socketId];
}

const checkHeartBeat = () => {
    for (const socketId in globalThis.game) {
        if (globalThis.game[socketId] && compareTimeWithNow(globalThis.game[socketId].lastHeartBeat) > 10000) {
            console.log(`Player ${socketId} has been disconnected!`);
            //Update result
            delete globalThis.game[socketId];
        }
    }
}

setInterval(checkHeartBeat, 1000);