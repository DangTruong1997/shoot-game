import { Response } from 'express';

export class BaseResponse {
    code: number;
    data: any;
    constructor(code?: number, data?: any) {
        this.code = code || 400;
        this.data = data || "";
    }
}

export const sendToClient = (res: Response, baseResponse: BaseResponse) => {
    return res.status(baseResponse.code).send(baseResponse.data);
}

export const getRandomArrElement = (arr: Array<any>) => arr[Math.floor(Math.random() * arr.length)];

export const copyObjectValue = (obj: any) => {
    let copyvalue = JSON.parse(JSON.stringify(obj));
    return copyvalue;
}

export const formatTime = (millTime: number) => {
    let minute: any = Math.floor(millTime / 60000);
    let second: any = Math.floor(millTime / 1000) - (minute * 60);
    minute = minute < 10 ? `0${minute}` : minute;
    second = second < 10 ? `0${second}` : second;
    return `${minute}:${second}`;
}

export const getCurrentTime = () => {
    return new Date().getTime();
}

export const compareTimeWithNow = (time: number) => {
    return getCurrentTime() - new Date(time).getTime();
}

export const errorHandlertask = async (task: Function, errHandler?: Function) => {
    try {
        await task();
    } catch (error) {
        console.log(error);
        if (errHandler) {
            await errHandler();
        }
    }
}

export const sortByProperty = (arrObj: any) => {
    return arrObj.sort((a: any, b: any) => (a.pos > b.pos) ? -1 : ((b.pos > a.pos) ? 1 : 0));
}