import { APP_CONFIG } from './../utils/configs';
import { BaseResponse, sendToClient } from './../utils/utils';
import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";

class APIController {

    private baseResponse = new BaseResponse(200);

    login = (req: Request, res: Response, next: NextFunction) => {
        this.baseResponse.data = {
            "turnNumber": 2,
            "uName": "Test",
            "uAvatar": "https://cdn.discordapp.com/emojis/582573374426054713.png?v=1",
            "id": Math.floor(Math.random() * 10001),
        }
        this.baseResponse.data.token = jwt.sign(this.baseResponse.data, APP_CONFIG.JWT_SECRET, { expiresIn: '3d' })
        return sendToClient(res, this.baseResponse);
    }

    getTurn = (req: Request, res: Response, next: NextFunction) => {
        const userId = req.query.userId.toString();
        if (!userId) {
            this.baseResponse.code = 500;
            this.baseResponse.data = "";
        } else {
            this.baseResponse.data = {
                "turnNumber": globalThis[userId] ? globalThis[userId].turnNumber : 2,
            }
        }
        return sendToClient(res, this.baseResponse);
    }

    // play = (req: Request, res: Response, next: NextFunction) => {
    //     if (req.session.player && req.session.player.turnNumber > 0) {
    //         this.baseResponse.data = { code: 200 };
    //     } else {
    //         this.baseResponse.data = { code: 401 };
    //     }
    //     return sendToClient(res, this.baseResponse);
    // }

}

export default APIController;