// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import CustomGameplay from "../Game/CustomGameplay";

const {ccclass, property} = cc._decorator;

@ccclass
export default class AudioPenController extends cc.Component {



    PlayAudioTrue()
    {
        this.node.getComponent(cc.AudioSource).clip = CustomGameplay.instance.audioTrue;
        this.node.getComponent(cc.AudioSource).play();
    }

    
    PlayAudioFalse()
    {
        this.node.getComponent(cc.AudioSource).clip = CustomGameplay.instance.audioFalse;
        this.node.getComponent(cc.AudioSource).play();
    }

    async loadImage(_sount: cc.Node, url) {
        cc.loader.loadRes(url, cc.AudioClip, function (err, audioClip) {

            cc.log(typeof audioClip);  // 'object'
        });
    }
}
