class SOCKET {

  constructor(url) {
    this.socket = io(url, {
      transports: ['websocket', 'polling'],
      pingTimeout: 30000,
      reconnectionAttempts: 3,
      reconnectionDelayMax: 1500,
      reconnectionDelay: 1000,
      forceNew: true
    });

    this.config();
  }

  config() {
    R.socket = this.socket;

    this.socket.on("disconnect", function (err) {
      if (R.scene && R.scene.scene.isActive('play') && R.statePlaying) {
        R.createPopupNotice(R.scene, "Kết nối mạng của bạn hiện không ổn định.\nĐang kết nối lại...", null, "Mất kết nối");
      }
    });

    this.socket.on("connect", function () {
      if (R.scene && R.scene.scene.isActive('play')) {
        if (R.scene.dialogNotice) {
          R.scene.dialogNotice.Close(R.scene);
        }
        R.socket.emit('reconnect-play');
      }
    });

    this.socket.on('reconnect', function (err) {
    });

    this.socket.on('error', function (err) {
      R.socket = null;
      R.createPopupNotice(R.scene, err, function (scene) {
        sleep(500);
        scene.scene.start('login');
      });
    });

    this.socket.on('reconnect_failed', function (err) {
      if (R.scene.matchingTimer) {
        R.scene.matchingTimer.remove();
      }
      R.createPopupNotice(R.scene, "Bạn đã bị mất kết nối với máy chủ, vui lòng kiểm tra tín hiệu mạng và thử lại", function () {
        return window.top.location.reload();
      });
    });

    this.socket.on('alert-login', function (data) {
      R.socket = null;
      R.createPopupNotice(R.scene, data, function (scene) {
        sleep(500);
        scene.scene.start('login');
      });
    });

    this.socket.on('invite-success', function (data) {
      R.scene.popupInviting = R.createPopupInviting(R.scene, data.msisdn);
    });

    this.socket.on('invite-error', function (data) {
      if (R.scene.popupInviting) {
        R.scene.popupInviting.Close(R.scene);
      }
      if (R.scene.createPopupInviteAgain) {
        R.scene.createPopupInviteAgain(data.message);
      }
    });

    this.socket.on('confirm-error', function (data) {
      if (R.scene.popupInviting) {
        R.scene.popupInviting.Close(R.scene);
      }
      R.createPopupConfirmError(R.scene, data.message);
    });

    this.socket.on('error-notice', function (data) {
      R.createPopupNotice(R.scene, data.message, function () {
        return true
      });
    });

    this.socket.on('invite-receive', function (data) {
      R.noticeInviteState = true;
      if (R.scene && R.scene.scene.isActive('home')) {
        R.scene.noticeIcon.visible = true;
      }
      R.createPopupNoticeWithButton(R.scene, `Người chơi ${data.msisdn} vừa gửi cho bạn một lời mời thách đấu với số điểm cược là ${data.point}. Bạn có đồng ý chơi game?`, `Đồng ý`, function (scene, self) {
        R.socket.emit('accept', {
          msisdn: data.msisdn
        });
        self.Close(scene);
      }, null, 100, "Lời mời")
    });

    this.socket.on("confirm-success", function (data) {
      if (R.user) {
        R.user.turn_bonus > 0 ? R.user.turn_bonus-- : R.user.turn--;
        R.user.point -= data.point;
        R.user.count_play_today++;
      }
      if (R.popup) {
        R.popup.visible = false;
      }
      if (R.scene.matchingTimer) {
        R.scene.matchingTimer.remove();
      }
      if (R.scene.timersocketJoinRoom) {
        clearInterval(R.scene.timersocketJoinRoom);
      }
      if (R.scene.timerWaitingConfirm) {
        clearInterval(R.scene.timerWaitingConfirm);
      }
      R.scene.waitingTimerCount = 3;
      R.rivalMsisdn = data.msisdn;
      R.createPopupNotice(R.scene, `Đã ghép cặp thành công với [color=#331F00][b]${data.opponent_name}[/b][/color].\n Chờ vào game (${R.scene.waitingTimerCount})`, null, null, true);
      R.scene.waitingTimer = R.scene.time.addEvent({
        delay: 1000,
        callback: function () {
          R.scene.waitingTimerCount--;
          if (R.scene.waitingTimerCount >= 0 && R.scene.dialogNotice) {
            R.scene.dialogNotice.getByName('content').text = `Đã ghép cặp thành công với [color=#331F00][b]${data.opponent_name}[/b][/color].\n Chờ vào game (${R.scene.waitingTimerCount})`;
          } else {
            R.scene.waitingTimer.remove();
            let mode = "Challenge";
            R.scene.scene.start('play', {
              mode: mode,
              firstWord: data.firstWord,
              initWord: data.initWord,
              opponentName: data.opponent_name,
              playerName: R.user ? R.user.name : R.tmpName,
              hiddenReportFlag: true
            });
          }
        },
        callbackScope: R.scene,
        loop: true
      });
    });

    this.socket.on('receive-word', function (data) {
      if (R.scene && R.scene.scene.isActive('play')) {
        let timestamp = parseInt(data.timestamp) || 0;
        R.scene.turnTimeHook = new Date().getTime() + 20000 - timestamp * 1000;
        R.scene.themeWord = data.word;
        R.scene.themeWordKey = `${R.scene.themeWord.split(" ")[1]} ` || null;
        R.scene.opponentWord.text = 'Từ thách đấu:';
        R.scene.opponentWordTxt.text = R.scene.themeWord;
        R.scene.playerWordInput.text = R.scene.themeWordKey;
        R.scene.opponentWordTxt.visible = true;
        R.scene.loadingEffect.visible = false;
        R.scene.playerWordInput.visible = true;
        R.scene.playerWordTxt.visible = false;
        R.scene.turn = true;
        R.scene.reportFlag.setInteractive();
        R.scene.reportFlag.setTexture("report_icon_on");
      }
    });

    this.socket.on('send-word-ok', function (data) {
      if (R.scene && R.scene.scene.isActive('play')) {
        let timestamp = parseInt(data.timestamp) || 0;
        R.scene.turnTimeHook = new Date().getTime() + 20000 - timestamp * 1000;
        R.scene.opponentWordTxt.visible = false;
        R.scene.loadingEffect.visible = true;
        R.scene.playerWordTxt.text = data.word;
        R.scene.playerWordTxt.visible = true;
        R.scene.playerWordInput.visible = false;
        R.scene.reportFlag.disableInteractive();
        R.scene.reportFlag.setTexture('report_icon_off');
      }
    });

    this.socket.on('win-game', function (data) {
      R.statePlaying = false;
      data.point = parseInt(data.point);
      if (R.user) {
        R.user.point += (data.point + data.point * 9 / 10);
      }
      if (R.scene && R.scene.scene.isActive('play')) {
        if (R.scene.primaryColor == 'green') {
          R.createPopupNoticeWithButton(R.scene, `Chúc mừng. Bạn đã chiến thắng đối thủ trong ${Math.floor(data.match_time / 60)} phút ${data.match_time % 60} giây và nhận về ${data.point + data.point * 9 / 10} điểm. Chơi tiếp thắng tiếp!`, "Chơi tiếp", function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, 100, "Thắng cuộc")
        } else {
          R.createPopupNoticeWithButton2(R.scene, `Chúc mừng. Bạn đã chiến thắng đối thủ trong ${Math.floor(data.match_time / 60)} phút ${data.match_time % 60} giây và nhận về ${data.point + data.point * 9 / 10} điểm. Chơi tiếp thắng tiếp!`, "Chơi tiếp", function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, "Mời lại", function (scene) {
            scene.scene.transition({
              target: "bet",
              data: {
                mode: "Challenge",
                initMsisdn: R.rivalMsisdn
              },
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, 100, "Thắng cuộc")
        }
      }
    });

    this.socket.on('lose-game', function (data) {
      R.statePlaying = false;
      if (R.scene && R.scene.scene.isActive('play')) {
        if (R.scene.primaryColor == 'green') {
          R.createPopupNoticeWithButton(R.scene, `Rất tiếc. Bạn đã để thua đối thủ trong ${Math.floor(data.match_time / 60)} phút ${data.match_time % 60} giây và mất đi ${data.point} điểm. Hãy cố gắng trong lần sau nhé!`, "Chơi tiếp", function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, 100, "Thua cuộc");
        } else {
          R.createPopupNoticeWithButton2(R.scene, `Rất tiếc. Bạn đã để thua đối thủ trong ${Math.floor(data.match_time / 60)} phút ${data.match_time % 60} giây và mất đi ${data.point} điểm. Hãy cố gắng trong lần sau nhé!`, "Chơi tiếp", function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, "Mời lại", function (scene) {
            scene.scene.transition({
              target: "bet",
              data: {
                mode: "Challenge",
                initMsisdn: R.rivalMsisdn
              },
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, 100, "Thua cuộc");
        }
      }
    });

    this.socket.on('send-word-system-err', function () {
      R.createPopupNotice(R.scene, `Hệ thống đã xảy ra lỗi, xin vui lòng thử lại.`, function () {
        return true;
      });
    });

    this.socket.on('report-notice', function (data) {
      if (R.scene && R.scene.scene.isActive('play')) {
        data.point = parseInt(data.point);
        let content = '';
        if (data.reporter == 0) {
          content = `Đối thủ đã báo cáo về từ [b][color=red]${data.word.toUpperCase()}[/color][/b] của bạn vì cho rằng từ không đúng.\nVui lòng chờ ít ngày trong lúc BQT đánh giá. Trong thời gian này ${data.point} điểm sẽ được giữ lại trên hệ thống. Từ [b][color=red]${data.word.toUpperCase()}[/color][/b] hợp lệ, bạn nhận về ${data.point + data.point * 9 / 10} điểm. Từ [b][color=red]${data.word.toUpperCase()}[/color][/b] không hợp lệ, bạn sẽ mất ${data.point} điểm. Chơi tiếp, thử thách tiếp`;
        } else {
          content = `Cảm ơn bạn đã gửi báo cáo về từ [b][color=red]${data.word.toUpperCase()}[/color][/b].\nVui lòng chờ ít ngày trong lúc BQT đánh giá. Trong thời gian này ${data.point} điểm sẽ được giữ lại trên hệ thống. Báo cáo đúng, bạn nhận về ${data.point + data.point * 9 / 10} điểm. Báo cáo không chính xác, bạn sẽ mất ${data.point} điểm. Chơi tiếp, thử thách tiếp!`;
        }
        if (data.randomMode) {
          content = `Từ [b][color=red]${data.word.toUpperCase()}[/color][/b] bạn vừa gửi không có trong từ điển. Hệ thống sẽ xem xét và thông báo lại sau khi có kết quả kiểm tra.`;
        }
        if (R.scene.primaryColor == "green") {
          R.createPopupNotice(R.scene, content, function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, "Dừng trận", true);
        } else {
          R.createPopupNoticeWithButton3(R.scene, content, 'Mời lại', function (scene) {
            scene.scene.transition({
              target: "bet",
              data: {
                mode: "Challenge",
                initMsisdn: R.rivalMsisdn
              },
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, 100, "Dừng trận", true)
        }
      }
    });

    this.socket.on('not-enough-point', function () {
      R.createPopupNoticeWithButton(R.scene, "Bạn không đủ điểm để cược. Vui lòng chọn số điểm cược khác hoặc chọn thêm điểm.\nSoạn VTN NC NAP10 gửi 9029 để có thêm 300 điểm.", "Thêm điểm", function () {
        R.openSMS(9029, "VTN NC NAP10");
      }, function () {
        return true;
      }, 100);
    });

    this.socket.on('not-enough-turn', function () {
      R.createPopupNoticeWithButton(R.scene, "Bạn không đủ lượt chơi. Vui lòng chọn mua thêm lượt chơi.\nSoạn VTN NC NAP10 gửi 9029 để có thêm 10 lượt chơi vào 300 điểm.", "Thêm lượt", function () {
        R.openSMS(9029, "VTN NC NAP10");
      }, function () {
        return true;
      }, 100);
    });

    this.socket.on('data-leaderboard', function (data) {
      if (R.scene.dialogLeaderboard && R.scene.dialogLeaderboard.visible) {
        R.scene.leaderboardDialogTemplate(data.users);
      }
    });

    this.socket.on('data-history', function (data) {
      if (R.scene.dialogHistory && R.scene.dialogHistory.visible) {
        R.historyPlay = data.invite;
        R.historyPoint = data.game;
        R.historyReport = data.report;
        R.scene.createTab1(R.historyPlay.slice(0, 10));
        R.scene.createTab2(R.historyPoint.slice(0, 10));
        R.scene.createTab3(R.historyReport.slice(0, 10));
        R.scene.setTab1Visible(true);
        R.scene.setTab2Visible(false);
        R.scene.setTab2Visible(false);
        R.scene.loadingHistoryTxt.visible = false;
      }
    });

    this.socket.on('match-random-success', function (data) {
      R.user.turn_bonus > 0 ? R.user.turn_bonus-- : R.user.turn--;
      R.user.point -= data.point;
      R.user.count_play_today++;
      if (R.popup) {
        R.popup.visible = false;
      }
      if (R.scene.matchingTimer) {
        R.scene.matchingTimer.remove();
      }
      R.scene.waitingTimerCount = 3;
      R.createPopupNotice(R.scene, `Đã ghép cặp thành công với [color=#331F00][b]${data.opponent_name}[/b][/color].\n Chờ vào game (${R.scene.waitingTimerCount})`, null, null, true);
      R.scene.waitingTimer = R.scene.time.addEvent({
        delay: 1000,
        callback: function () {
          R.scene.waitingTimerCount--;
          if (R.scene.waitingTimerCount >= 0 && R.scene.dialogNotice) {
            R.scene.dialogNotice.getByName('content').text = `Đã ghép cặp thành công với [color=#331F00][b]${data.opponent_name}[/b][/color].\n Chờ vào game (${R.scene.waitingTimerCount})`;
          } else {
            R.scene.waitingTimer.remove();
            let mode = "Random";
            R.scene.scene.start('play', {
              mode: mode,
              firstWord: data.first_word,
              opponentName: data.opponent_name,
              playerName: R.user.name
            });
          }
        },
        callbackScope: R.scene,
        loop: true
      });
    });

    this.socket.on('join-room-invite-link-notice', function (data) {
      var self = R.scene;
      if (!self.socketJoinRoomPopup || !self.socketJoinRoomPopup.visible) {
        R.playSfx('Popup');
        let time = 180;

        var closeBtn = R.createImage(self, 300, -500, 'close_btn').setInteractive().on('pointerdown', function () {
          R.playSfx('Button');
          clearInterval(self.timersocketJoinRoom);
          self.socketJoinRoomPopup.Close(self);
        });

        self.socketJoinRoomBtn = R.createImage(self, 0, 300, "popup_green_btn_normal").setInteractive().on('pointerdown', function () {
          if (R.socket && R.socket.nsp != "/room") {
            R.socket.disconnect();
          }
          new SOCKET(R.domain + "room");
          R.socket.emit('confirm-join-room', {
            msisdn: data.msisdn
          });
          self.socketJoinRoomPopup.Close(self);
        });

        self.socketJoinRoomBtnTxt = R.createText(self, 0, 300, 32, R.strings.join_room, "#fff", "#375100", 7);

        self.socketJoinRoomPopup = new R.CustomDialog(self, R.halfGameWidth, R.halfGameHeight, 'popup_bg_small', true);
        self.socketJoinRoomPopup.depth = 101;
        self.socketJoinRoomPopupTitle = R.createText(self, 0, -400, 50, R.strings.join_room, '', '', '', true);
        self.socketJoinRoomContent = R.createText(self, 0, 0, 40, `${data.name} đã chấp nhận tham gia phòng đấu của bạn. Bạn còn ${time}s để vào phòng.\n\nLưu ý rằng phần chơi này không được tính điểm và xếp hạng.`, "#000", false, 0, false, true, true, "center", 100, 450);
        self.socketJoinRoomPopup.add([self.socketJoinRoomPopupTitle, self.socketJoinRoomContent, self.socketJoinRoomBtn, self.socketJoinRoomBtnTxt, closeBtn]);
        self.socketJoinRoomPopup.Open(self);
        self.timersocketJoinRoom = setInterval(function () {
          time--;
          if (self.socketJoinRoomContent) {
            if (time < 0) {
              clearInterval(self.timersocketJoinRoom);
              self.socketJoinRoomContent.text = `Đã hết thời gian để chấp nhận vào phòng.`;
              self.socketJoinRoomBtnTxt.text = R.strings.main_screen;
              self.socketJoinRoomBtn.setTexture('popup_green_btn_large');
              self.socketJoinRoomBtn.off('pointerdown');
              self.socketJoinRoomBtn.on('pointerdown', function () {
                self.socketJoinRoomPopup.Close(self);
                self.scene.start('home');
              });
            } else {
              self.socketJoinRoomContent.text = `${data.name} đã chấp nhận tham gia phòng đấu của bạn. Bạn còn ${time}s để vào phòng.\n\nLưu ý rằng phần chơi này không được tính điểm và xếp hạng.`;
            }
          }
        }, 1000);
      }
    });

    this.socket.on("confirm-success-link", function (data) {
      if (R.user) {
        R.user.turn_bonus > 0 ? R.user.turn_bonus-- : R.user.turn--;
        R.user.point -= data.point;
        R.user.count_play_today++;
      }
      if (R.popup) {
        R.popup.visible = false;
      }
      if (R.scene.matchingTimer) {
        R.scene.matchingTimer.remove();
      }
      if (R.scene.timersocketJoinRoom) {
        clearInterval(R.scene.timersocketJoinRoom);
      }
      if (R.scene.timerWaitingConfirm) {
        clearInterval(R.scene.timerWaitingConfirm);
      }
      R.scene.waitingTimerCount = 3;
      R.rivalMsisdn = data.msisdn;
      R.createPopupNotice(R.scene, `Đã ghép cặp thành công với [color=#331F00][b]${data.opponent_name}[/b][/color].\n Chờ vào game (${R.scene.waitingTimerCount})`, null, null, true);
      R.scene.waitingTimer = R.scene.time.addEvent({
        delay: 1000,
        callback: function () {
          R.scene.waitingTimerCount--;
          if (R.scene.waitingTimerCount >= 0 && R.scene.dialogNotice) {
            R.scene.dialogNotice.getByName('content').text = `Đã ghép cặp thành công với [color=#331F00][b]${data.opponent_name}[/b][/color].\n Chờ vào game (${R.scene.waitingTimerCount})`;
          } else {
            R.scene.waitingTimer.remove();
            let mode = "Challenge";
            R.scene.scene.start('play', {
              mode: mode,
              firstWord: data.firstWord,
              initWord: data.initWord,
              opponentName: data.opponent_name,
              playerName: R.user ? R.user.name : R.tmpName,
              hiddenReportFlag: true
            });
          }
        },
        callbackScope: R.scene,
        loop: true
      });
    });

    this.socket.on('win-game-link', function (data) {
      R.statePlaying = false;
      data.point = parseInt(data.point);
      if (R.user) {
        R.user.point += (data.point + data.point * 9 / 10);
      }
      if (R.scene && R.scene.scene.isActive('play')) {
        if (R.user) {
          R.createPopupNoticeWithButtonLong(R.scene, `Chúc mừng. Bạn đã chiến thắng đối thủ trong ${Math.floor(data.match_time / 60)} phút ${data.match_time % 60} giây.\n\nĐể nhận được những phần quà hấp dẫn, hãy mời đấu ngẫu nhiên hoặc mời thuê bao đăng kí Uwin để cược điểm và thi đấu.`, "Màn hình chính", function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, 100, "Kết thúc")
        } else {
          R.createPopupEndGameNotLoginInviteLink(R.scene, `Chúc mừng. Bạn đã chiến thắng đối thủ trong ${Math.floor(data.match_time / 60)} phút ${data.match_time % 60} giây.\n\nHãy đăng nhập ugame để chơi game thỏa thích và có cơ hội nhận nhiều phần quà giá trị.`, "Đăng nhập", function (scene) {
            scene.scene.transition({
              target: "login",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, 100, "Kết thúc")
        }
      }
    });

    this.socket.on('lose-game-link', function (data) {
      R.statePlaying = false;
      if (R.scene && R.scene.scene.isActive('play')) {
        if (R.user) {
          R.createPopupNoticeWithButtonLong(R.scene, `Rất tiếc. Bạn đã để thua đối thủ trong ${Math.floor(data.match_time / 60)} phút ${data.match_time % 60} giây.\n\nĐể nhận được những phần quà hấp dẫn, hãy mời đấu ngẫu nhiên hoặc mời thuê bao đăng kí Uwin để cược điểm và thi đấu.`, "Màn hình chính", function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, 100, "Kết thúc");
        } else {
          R.createPopupEndGameNotLoginInviteLink(R.scene, `Rất tiếc. Bạn đã để thua đối thủ trong ${Math.floor(data.match_time / 60)} phút ${data.match_time % 60} giây.\n\nHãy đăng nhập ugame để chơi game thỏa thích và có cơ hội nhận nhiều phần quà giá trị.`, "Đăng nhập", function (scene) {
            scene.scene.transition({
              target: "login",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, function (scene) {
            scene.scene.transition({
              target: "home",
              duration: 0,
              onUpdate: scene.destroy(),
              onUpdateScope: scene
            });
          }, 100, "Kết thúc");
        }
      }
    });

  }
}