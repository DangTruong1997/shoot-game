// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameManager from "../Manager/GameManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class APIController extends cc.Component {

    public static instance: APIController;


    start() {
        APIController.instance = this;
    }

    

    callAPIPost(
        data: any,
        api: any,
        successCallback: any,
        errorCallback: any
    ) {

        let xhr = new XMLHttpRequest();
        var url = GameManager.url + api;

        var check: boolean = false;
        xhr.onreadystatechange = function () {


            if (xhr.status == 200) {
                if (successCallback && typeof successCallback == "function")
                    if (!check) {
                        successCallback(xhr.responseText);
                        check = true;
                    }
            } else {
                if (errorCallback && typeof errorCallback == "function")
                    errorCallback();

            }

        };
        xhr.open("POST", url, true);
        xhr.send(data);
    }
}
