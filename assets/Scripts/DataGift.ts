// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator; 

@ccclass
export default class DataGift {

    @property(Number)
    typeGift: Number = 0;

    @property(cc.SpriteFrame)
    image: cc.SpriteFrame = null;

    @property(Number)
    score: Number = 0;
 
    @property(Number)
    heart: Number = 0;

    @property(Number)
    ratio: Number = 0;
}
