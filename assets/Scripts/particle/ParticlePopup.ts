// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class ParticlePopup extends cc.Component {

    @property(cc.Node)
    table: cc.Node = null;

    onEnable() {
        this.table.active = true
        this.table.setScale(0, 0);
        var mto3 = cc.scaleTo(0.6, 1);
        this.table.runAction(mto3);

    }

    OnClick_Close() {
        
        var param = this;
        var mto = cc.scaleTo(0.4, 0);
        var callback = cc.callFunc(function () {
            param.node.active = false;
        })
        this.table.runAction(cc.sequence(mto, callback));
    }
}
