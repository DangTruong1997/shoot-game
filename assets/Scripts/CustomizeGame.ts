// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import ConfigString from "./ConfigString";
import LoadJsonController from "./LoadJson/LoadJsonController";
import GameManager from "./Manager/GameManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CustomizeGame extends cc.Component {

    public static instance: CustomizeGame;

    @property
    SpriteMain: string = 'Node Main';

    @property(cc.Node)
    spriteBG: cc.Node = null; // node background

    @property(cc.Node)
    spriteLogo: cc.Node = null; // node logo

    
    @property(cc.Node)
    txtCountPlayGame: cc.Node = null;//time

    @property
    ButtonMain: string = 'Button Main';

    @property(cc.Node)
    btnPlay: cc.Node = null; // node button chơi ngay

    @property(cc.Node)
    btnTheLe: cc.Node = null; // node button thể lệ

    @property(cc.Node)
    btnPhanThuong: cc.Node = null; // node button phần thưởng


    @property
    Sound: string = 'Sound';

    @property(cc.AudioSource)
    AudioSourceBG: cc.AudioSource = null;

    //gameover1
    @property(cc.Node)
    spriteBGInPopupGameOver1 : cc.Node = null;

    @property(cc.Node)
    btnBtnPlayAgainInPopupGameOver1 : cc.Node = null;

    @property(cc.Node)
    txtKeyScorePopupGameOver1: cc.Node = null; //key score

    @property(cc.Node)
    txtScorePopupGameOver1: cc.Node = null; //score

    @property(cc.Node)
    txtKeyTimePopupGameOver1: cc.Node = null;//key time

    @property(cc.Node)
    txtTimePopupGameOver1: cc.Node = null;//time

    @property(cc.Node)
    txtCountPlayPopupGameOver1: cc.Node = null;//Count Play

    //gameover2
    @property(cc.Node)
    spriteBGInPopupGameOver2 : cc.Node = null;

    @property(cc.Node)
    btnBtnCloseAgainInPopupGameOver2 : cc.Node = null;

    @property(cc.Node)
    txtKeyScorePopupGameOver2: cc.Node = null;//key score

    @property(cc.Node)
    txtScorePopupGameOver2: cc.Node = null;//score

    @property(cc.Node)
    txtKeyTimePopupGameOver2: cc.Node = null;//key time

    @property(cc.Node)
    txtTimePopupGameOver2: cc.Node = null;//time

    //Pause
    @property(cc.Node)
    btnBtnPlayInPopupPause : cc.Node = null;

    @property(cc.Font)
    txtFont: cc.Font = null;

    start() {
        //image main
        CustomizeGame.instance.Custom(CustomizeGame.instance);
    }

    onLoad(){
        CustomizeGame.instance = this;
        CustomizeGame.instance.AddColorTextaa(CustomizeGame.instance.txtKeyScorePopupGameOver1, this.txtFont, 80, GameManager.dataCustom.color_text.popup);

        CustomizeGame.instance.AddColorTextaa(CustomizeGame.instance.txtKeyScorePopupGameOver1, this.txtFont, 80, GameManager.dataCustom.color_text.popup);
        CustomizeGame.instance.AddColorTextaa(CustomizeGame.instance.txtKeyTimePopupGameOver1, this.txtFont, 80, GameManager.dataCustom.color_text.popup);
        CustomizeGame.instance.AddColorTextaa(CustomizeGame.instance.txtKeyScorePopupGameOver2, this.txtFont, 80, GameManager.dataCustom.color_text.popup);
        CustomizeGame.instance.AddColorTextaa(CustomizeGame.instance.txtKeyTimePopupGameOver2, this.txtFont, 80, GameManager.dataCustom.color_text.popup);

        CustomizeGame.instance.AddColorTextaa(CustomizeGame.instance.txtScorePopupGameOver1, this.txtFont, 80, GameManager.dataCustom.color_text.popup);
        CustomizeGame.instance.AddColorTextaa(CustomizeGame.instance.txtTimePopupGameOver1, this.txtFont, 80, GameManager.dataCustom.color_text.popup);
        CustomizeGame.instance.AddColorTextaa(CustomizeGame.instance.txtScorePopupGameOver2, this.txtFont, 80, GameManager.dataCustom.color_text.popup);
        CustomizeGame.instance.AddColorTextaa(CustomizeGame.instance.txtTimePopupGameOver2, this.txtFont, 80, GameManager.dataCustom.color_text.popup);
        CustomizeGame.instance.AddColorTextaa(CustomizeGame.instance.txtCountPlayPopupGameOver1, this.txtFont, 50, GameManager.dataCustom.color_text.popup);

        CustomizeGame.instance.AddColorTextaa(CustomizeGame.instance.txtCountPlayGame, this.txtFont, 50, GameManager.dataCustom.color_text.body);
    }

    urlParam(name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.search);
        return (results !== null) ? results[1] || 0 : false;
    }

    Custom(param) {
        LoadJsonController.instance.LoadImg(param.spriteBG, GameManager.dataCustom.sprite.menu.srcImgBG)
        LoadJsonController.instance.LoadImg(param.spriteLogo, GameManager.dataCustom.sprite.menu.srcImgLogo)

        LoadJsonController.instance.LoadButton(param.btnPlay, GameManager.dataCustom.sprite.menu.srcImgBtnPlay);
        LoadJsonController.instance.LoadButton(param.btnTheLe, GameManager.dataCustom.sprite.menu.srcImgBtnTheLe);
        LoadJsonController.instance.LoadButton(param.btnPhanThuong, GameManager.dataCustom.sprite.menu.srcImgBtnPhanThuong);

        LoadJsonController.instance.LoadImg(param.spriteBGInPopupGameOver1, GameManager.dataCustom.sprite.game.srcImgBGInPopupGameOver1)
        LoadJsonController.instance.LoadButton(param.btnBtnPlayAgainInPopupGameOver1, GameManager.dataCustom.sprite.game.srcImgBtnPlayAgainInPopupGameOver1);

        LoadJsonController.instance.LoadImg(param.spriteBGInPopupGameOver2, GameManager.dataCustom.sprite.game.srcImgBGInPopupGameOver2)
        LoadJsonController.instance.LoadButton(param.btnBtnCloseAgainInPopupGameOver2, GameManager.dataCustom.sprite.game.srcImgBtnCloseAgainInPopupGameOver2);

        LoadJsonController.instance.LoadButton(param.btnBtnPlayInPopupPause, GameManager.dataCustom.sprite.game.srcImgBtnPlayInPopupPause);
       
        LoadJsonController.instance.loadSound(param.AudioSourceBG, GameManager.dataCustom.audio.srcAudioBG)
    }

    AddColorTextaa(txt : cc.Node, font : cc.Font, fontSize : number, color : string)
    {
        txt.color = new cc.Color().fromHEX(color);
        txt.addComponent(cc.Label);
        txt.getComponent(cc.Label).font = font;
        txt.getComponent(cc.Label).fontSize = fontSize;
        txt.getComponent(cc.Label).lineHeight = fontSize;
    }

    ChangeSprite(sprite: cc.Node, image: cc.SpriteFrame) {
        sprite.getComponent(cc.Sprite).spriteFrame = image;
    }

    ChangeSpriteButon(button: cc.Node, image: cc.SpriteFrame) {
        button.getComponent(cc.Button).disabledSprite = image;
        button.getComponent(cc.Button).hoverSprite = image;
        button.getComponent(cc.Button).normalSprite = image;
        button.getComponent(cc.Button).pressedSprite = image;
    }

    ChangeSound(AudioSource: cc.AudioSource, AudioClip: cc.AudioClip) {
        AudioSource.clip = AudioClip;
    }
}
