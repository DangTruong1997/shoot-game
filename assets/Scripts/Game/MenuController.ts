// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import ConfigString from "../ConfigString";
import GameManager from "../Manager/GameManager";
import GameController from "./GameController";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MenuController extends cc.Component {


    public static instance: MenuController;

    @property(cc.Node)
    popupPause: cc.Node = null;

    @property(cc.Node)
    popupGameOver1: cc.Node = null;

    @property(cc.Node)
    popupGameOver2: cc.Node = null;

    @property(cc.Node)
    txtTurn: cc.Node = null;

    start() {
        MenuController.instance = this;
        MenuController.instance.HashChange();
        function locationHashChanged() {
            if (!GameManager.login) {
                MenuController.instance.HashChange();
            }
        }
        window.onhashchange = locationHashChanged;
    }
    private clickMenuEvent = new CustomEvent("clickMenu");
    showOutsidePopup(name?: 'popup_left' | 'popup_right') {
        if (window.top) {
            window.parent.postMessage({ btnName: name }, '*')
        } else {
            this.clickMenuEvent.dispatchEvent({ btnName: name });
        }
    }
    HashChange() {
        if (location.hash === ConfigString.hashtag_pause) {
            MenuController.instance.popupGameOver1.active = false;
            MenuController.instance.popupGameOver2.active = false;
            MenuController.instance.popupPause.active = true;
        }

        if (location.hash === ConfigString.hashtag_gameover1) {
            MenuController.instance.popupGameOver1.active = true;
            MenuController.instance.popupGameOver2.active = false;
            MenuController.instance.popupPause.active = false;
        }

        if (location.hash === ConfigString.hashtag_gameover2) {
            MenuController.instance.popupGameOver1.active = false;
            MenuController.instance.popupGameOver2.active = true;
            MenuController.instance.popupPause.active = false;
        }
    }


    onEnable() {
        GameManager.score = GameManager.constScore;
        GameManager.countHeart = GameManager.constCountHeart;
        this.GetTurn();
    }

    GetTurn() {
        this.txtTurn.getComponent(cc.Label).string = ConfigString.strBeforeCountPlay + GameManager.countPlay + ConfigString.strAfterCountPlay;
    }

    OnClick_Play() {
        if (GameManager.countPlay > 0) {
            GameManager.countPlay--;
            cc.director.loadScene("Game");
        }
    }

    OnClick_TheLe() {
        this.showOutsidePopup('popup_left')
    }
    OnClick_PhanThuong() {
        this.showOutsidePopup('popup_right')
    }

    OnClick_ClosePause() {
        this.popupPause.active = false;
    }
}
