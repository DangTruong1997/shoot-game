import ConfigTag from "../ConfigTag";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameManager from "../Manager/GameManager";
import GameController from "./GameController";
import Virus1Controller from "./Virus1Controller";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PenController extends cc.Component {

    move: boolean = false;

    die: boolean = false;

    checkEnter: boolean = false;
    timestar;


    onLoad() {

        this.node.parent.on(cc.Node.EventType.TOUCH_START, (event) => {
            if (!this.die) {

                if (!this.move && !GameController.instance.checkPause) {
                    this.move = true;
                    this.timestar = new Date().getTime();

                    if (GameManager.login) {
                        GameController.instance.Shoot();
                    }
                }
                this.die = true;
            };

        });
    }



    live: boolean = false;

    update() {
        if (this.move && !GameController.instance.checkPause && !this.checkEnter) {
            this.node.y += GameManager.speedMove;
            if (this.node.y >= 1300) {
                if (!this.live) {
                    this.node.destroy();
                    GameController.instance.OnClick_CreatePen();
                    if (!GameManager.login)
                        GameController.instance.PlusHeart(-1);
                    else {

                    }
                    this.live = true;
                }
            }
        }
    }

    Enter(_node, time) {

        setTimeout(() => {
            _node.getComponent(Virus1Controller).stop();
            this.move = false;
            this.node.parent = _node;
            this.node.position = new cc.Vec3(0, -150, 0);
            _node.getComponent(Virus1Controller).ActionAnimation();
            // GameController.instance.PlusScore(_node.getComponent(Virus1Controller).score);
            // GameController.instance.PlusHeart(_node.getComponent(Virus1Controller).countHeart);
            GameController.instance.EventCreate();
            this.node.group = "penDie";
            _node.group = "enemyDie";
        }, time);
    }

    onCollisionEnter(other, self) {
        if (!GameController.instance.checkPause) {
            if (other.tag == ConfigTag.tagVirus) {
                if (!this.checkEnter && !GameManager.login) {
                    other.node.getComponent(Virus1Controller).stop();
                    this.move = false;
                    this.node.parent = other.node;
                    this.node.position = new cc.Vec3(0, -100, 0);
                    other.node.getComponent(Virus1Controller).ActionAnimation();
                    GameController.instance.PlusScore(other.node.getComponent(Virus1Controller).score);
                    GameController.instance.PlusHeart(other.node.getComponent(Virus1Controller).countHeart);
                    GameController.instance.EventCreate();
                    this.node.group = "penDie";
                    other.node.group = "enemyDie";
                    this.checkEnter = true;
                }
            }
        }

    }


}
