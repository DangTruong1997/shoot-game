// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import ConfigString from "../ConfigString";
import LoadJsonController from "../LoadJson/LoadJsonController";
import GameManager from "../Manager/GameManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameOverController extends cc.Component {

    @property(cc.Label)
    keyScore: cc.Label = null;

    @property(cc.Label)
    keyTime: cc.Label = null;

    start() {
        LoadJsonController.instance.loadText(this.keyScore, ConfigString.strKeyScore);
        LoadJsonController.instance.loadText(this.keyTime, ConfigString.strKeytime);
    }

}
