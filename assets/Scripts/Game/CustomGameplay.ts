// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import ConfigString from "../ConfigString";
import DataGift from "../DataGift";
import LoadJsonController from "../LoadJson/LoadJsonController";
import GameManager from "../Manager/GameManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CustomGameplay extends cc.Component {

    public static instance: CustomGameplay;

    public countCreateNewAnim: number; //sau bao nhiêu lần va chạm với thành thì tạo gift mới

    public cycleMin: number; //chu trình di chuyển ngắn nhất

    public cycleMax: number; //chu trình di chuyển ngắn nhất

    public deltaCycle: number; //chu trình sau khoảng thời gian

    public deltaTimeCycle: number; //khoảng thời gian giảm chu trình


    public ratioTwoGift: number; // phần trăm xuất hiện 2 gift

    cycle: number = 0; //thời gian 1 chu trình di chuyển


    //custom image
    @property
    SpriteMain: string = 'Node Main';

    @property(cc.Node)
    spriteBG: cc.Node = null; // node background

    @property(cc.Node)
    spriteHeart: cc.Node = null; // node Heart

    @property(cc.Node)
    spriteAvatar: cc.Node = null; // node Heart

    @property
    ButtonMain: string = 'Button Game';

    @property(cc.Node)
    btnPause: cc.Node = null; // node button chơi ngay

    @property(cc.Node)
    btnBtnPlayInPopupPause: cc.Node = null; // node button chơi ngay

    @property
    popupGameOver1: string = 'Image Popup GameOver1';

    @property(cc.Node)
    spriteBGInPopupGameOver1: cc.Node = null; // node background

    @property(cc.Node)
    btnBtnPlayAgainInPopupGameOver1: cc.Node = null; // node button chơi ngay

    @property(cc.Node)
    txtKeyScorePopupGameOver1: cc.Node = null; //key score

    @property(cc.Node)
    txtScorePopupGameOver1: cc.Node = null; //score

    @property(cc.Node)
    txtKeyTimePopupGameOver1: cc.Node = null;//key time

    @property(cc.Node)
    txtTimePopupGameOver1: cc.Node = null;//time

    @property(cc.Node)
    txtCountPlayPopupGameOver1: cc.Node = null;//Count Play


    @property
    popupGameOver2: string = 'Image Popup GameOver1';

    @property(cc.Node)
    spriteBGInPopupGameOver2: cc.Node = null; // node background

    @property(cc.Node)
    btnBtnCloseAgainInPopupGameOver2: cc.Node = null; // node button chơi ngay

    @property(cc.Node)
    txtKeyScorePopupGameOver2: cc.Node = null;//key score

    @property(cc.Node)
    txtScorePopupGameOver2: cc.Node = null;//score

    @property(cc.Node)
    txtKeyTimePopupGameOver2: cc.Node = null;//key time

    @property(cc.Node)
    txtTimePopupGameOver2: cc.Node = null;//time

    @property
    Sound: string = 'Sound';

    @property(cc.AudioSource)
    AudioBG: cc.AudioSource = null;

    @property(cc.AudioSource)
    AudioPen: cc.AudioSource = null;


    @property
    ColorText: string = 'Color Text';

    @property(cc.Node)
    txtScore: cc.Node = null;

    @property(cc.Node)
    txtHeart: cc.Node = null;

    @property(cc.Font)
    txtFont: cc.Font = null;
    onLoad() {
        CustomGameplay.instance = this;
        CustomGameplay.instance.AddColorText(this.txtScore, this.txtFont, 80, GameManager.dataCustom.color_text.top);
        CustomGameplay.instance.AddColorText(this.txtHeart, this.txtFont, 80, GameManager.dataCustom.color_text.top);

        CustomGameplay.instance.AddColorText(CustomGameplay.instance.txtKeyScorePopupGameOver1, this.txtFont, 80, GameManager.dataCustom.color_text.popup);
        CustomGameplay.instance.AddColorText(CustomGameplay.instance.txtKeyTimePopupGameOver1, this.txtFont, 80, GameManager.dataCustom.color_text.popup);
        CustomGameplay.instance.AddColorText(CustomGameplay.instance.txtKeyScorePopupGameOver2, this.txtFont, 80, GameManager.dataCustom.color_text.popup);
        CustomGameplay.instance.AddColorText(CustomGameplay.instance.txtKeyTimePopupGameOver2, this.txtFont, 80, GameManager.dataCustom.color_text.popup);

        CustomGameplay.instance.AddColorText(CustomGameplay.instance.txtScorePopupGameOver1, this.txtFont, 80, GameManager.dataCustom.color_text.popup);
        CustomGameplay.instance.AddColorText(CustomGameplay.instance.txtTimePopupGameOver1, this.txtFont, 80, GameManager.dataCustom.color_text.popup);
        CustomGameplay.instance.AddColorText(CustomGameplay.instance.txtScorePopupGameOver2, this.txtFont, 80, GameManager.dataCustom.color_text.popup);
        CustomGameplay.instance.AddColorText(CustomGameplay.instance.txtTimePopupGameOver2, this.txtFont, 80, GameManager.dataCustom.color_text.popup);
        CustomGameplay.instance.AddColorText(CustomGameplay.instance.txtCountPlayPopupGameOver1, this.txtFont, 50, GameManager.dataCustom.color_text.popup);
    }

    AddColorText(txt: cc.Node, font: cc.Font, fontSize: number, color: string) {
        txt.color = new cc.Color().fromHEX(color);
        txt.addComponent(cc.Label);
        txt.getComponent(cc.Label).font = font;
        txt.getComponent(cc.Label).fontSize = fontSize;
        txt.getComponent(cc.Label).lineHeight = fontSize;
    }

    start() {
        this.Custom();
        this.cycle = this.cycleMax;
        this.IntervelTime();
    }

    Custom() {

        //parameter
        CustomGameplay.instance.countCreateNewAnim = GameManager.dataCustom.config_string.countCreateNewAnim;
        CustomGameplay.instance.cycleMin = GameManager.dataCustom.config_string.cycleMin;
        CustomGameplay.instance.cycleMax = GameManager.dataCustom.config_string.cycleMax;
        CustomGameplay.instance.deltaCycle = GameManager.dataCustom.config_string.deltaCycle;
        CustomGameplay.instance.deltaTimeCycle = GameManager.dataCustom.config_string.deltaTimeCycle;
        CustomGameplay.instance.ratioTwoGift = GameManager.dataCustom.config_string.ratioTwoGift;

        LoadJsonController.instance.LoadImg(CustomGameplay.instance.spriteBG, GameManager.dataCustom.sprite.game.srcImgBG)
        LoadJsonController.instance.LoadImg(CustomGameplay.instance.spriteHeart, GameManager.dataCustom.sprite.game.srcImgHeart)
        LoadJsonController.instance.LoadImg(CustomGameplay.instance.spriteAvatar, GameManager.dataPlayer.uAvatar)

        LoadJsonController.instance.LoadButton(CustomGameplay.instance.btnPause, GameManager.dataCustom.sprite.game.srcImgBtnPause);
        LoadJsonController.instance.LoadButton(CustomGameplay.instance.btnBtnPlayInPopupPause, GameManager.dataCustom.sprite.game.srcImgBtnPlayInPopupPause);

        LoadJsonController.instance.LoadImg(CustomGameplay.instance.spriteBGInPopupGameOver1, GameManager.dataCustom.sprite.game.srcImgBGInPopupGameOver1)
        LoadJsonController.instance.LoadButton(CustomGameplay.instance.btnBtnPlayAgainInPopupGameOver1, GameManager.dataCustom.sprite.game.srcImgBtnPlayAgainInPopupGameOver1);
        LoadJsonController.instance.loadText(CustomGameplay.instance.txtKeyScorePopupGameOver1.getComponent(cc.Label), GameManager.dataCustom.config_content_popup.strKeyScore)
        LoadJsonController.instance.loadText(CustomGameplay.instance.txtKeyTimePopupGameOver1.getComponent(cc.Label), GameManager.dataCustom.config_content_popup.strKeytime)

        LoadJsonController.instance.LoadImg(CustomGameplay.instance.spriteBGInPopupGameOver2, GameManager.dataCustom.sprite.game.srcImgBGInPopupGameOver2)
        LoadJsonController.instance.LoadButton(CustomGameplay.instance.btnBtnCloseAgainInPopupGameOver2, GameManager.dataCustom.sprite.game.srcImgBtnCloseAgainInPopupGameOver2);
        LoadJsonController.instance.loadText(CustomGameplay.instance.txtKeyScorePopupGameOver2.getComponent(cc.Label), GameManager.dataCustom.config_content_popup.strKeyScore)
        LoadJsonController.instance.loadText(CustomGameplay.instance.txtKeyTimePopupGameOver2.getComponent(cc.Label), GameManager.dataCustom.config_content_popup.strKeytime)

        LoadJsonController.instance.loadSound(CustomGameplay.instance.AudioBG, GameManager.dataCustom.audio.srcAudioBG);

        LoadJsonController.instance.loadText(CustomGameplay.instance.txtScore.getComponent(cc.Label), GameManager.dataCustom.startScore)
        LoadJsonController.instance.loadText(CustomGameplay.instance.txtHeart.getComponent(cc.Label), GameManager.dataCustom.startHeart)

    }

    PlayAudioHitTrue() {
        LoadJsonController.instance.loadSound(CustomGameplay.instance.AudioPen, GameManager.dataCustom.audio.srcAudioTrue);
    }

    PlayAudioHitFalse() {
        LoadJsonController.instance.loadSound(CustomGameplay.instance.AudioPen, GameManager.dataCustom.audio.srcAudioFalse);
    }

    async loadSound(audioSource: cc.AudioSource, url) {
        cc.loader.load(url, (err, audioClip) => {
            this.ChangeSound(audioSource, audioClip);
        });
    }

    ChangeSpeed() {

        if (this.cycle - this.deltaCycle > this.cycleMin) {
            this.cycle -= this.deltaCycle;
        }
        else {
            this.cycle = this.cycleMin;
            this.ClearIntervelTime();
        }
    }

    intervel;

    ClearIntervelTime() {
        clearInterval(this.intervel);
    }

    IntervelTime() {
        this.ClearIntervelTime();
        this.intervel = setInterval(() => {
            this.ChangeSpeed();
        }, this.deltaTimeCycle * 1000);
    }

    ChangeSprite(sprite: cc.Node, image: cc.SpriteFrame) {
        // sprite.setContentSize(image.getOriginalSize());
        sprite.getComponent(cc.Sprite).spriteFrame = image;
    }

    ChangeSpriteButon(button: cc.Node, image: cc.SpriteFrame) {
        // button.setContentSize(image.getOriginalSize());
        button.getComponent(cc.Button).disabledSprite = image;
        button.getComponent(cc.Button).hoverSprite = image;
        button.getComponent(cc.Button).normalSprite = image;
        button.getComponent(cc.Button).pressedSprite = image;
    }

    ChangeSound(AudioSource: cc.AudioSource, AudioClip: cc.AudioClip) {
        AudioSource.clip = AudioClip;
        AudioSource.play();
    }

}
