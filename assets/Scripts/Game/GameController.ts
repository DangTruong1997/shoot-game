// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import ConfigString from "../ConfigString";
import LoadJsonController from "../LoadJson/LoadJsonController";
import GameManager from "../Manager/GameManager";
import SocketController from "../Socket/SocketController";
import AudioPenController from "../Sound/AudioPenController";
import CustomGameplay from "./CustomGameplay";
import PenController from "./PenController";
import Virus1Controller from "./Virus1Controller";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameController extends cc.Component {

    public static instance: GameController;

    @property(cc.Prefab)
    prefabParent: cc.Prefab = null;

    @property(cc.Node)
    parent: cc.Node = null;

    @property(cc.Prefab)
    prefabPen: cc.Prefab = null;

    @property(cc.Prefab)
    virus2: cc.Prefab = null;

    @property(cc.Prefab)
    virus3: cc.Prefab = null;

    @property(cc.Vec2)
    posVirus2: cc.Vec2 = null;

    @property(cc.Vec2)
    posVirus3: cc.Vec2 = null;

    countVirus: number = 1;

    arrGiftSocket: cc.Node[] = [];

    parentVirus: cc.Node = null;

    //UI

    @property(cc.Node)
    txtScore: cc.Node = null;

    @property(cc.Node)
    txtHeart: cc.Node = null;

    @property(cc.Node)
    gameOver1: cc.Node = null;

    @property(cc.Node)
    gameOver2: cc.Node = null;

    @property(cc.Node)
    popupPause: cc.Node = null;

    @property(cc.Node)
    txtTurnGameOver: cc.Node = null;

    @property(cc.Node)
    txtScoreGameOver1: cc.Node = null;

    @property(cc.Node)
    txtTimeGameOver1: cc.Node = null;

    @property(cc.Node)
    txtScoreGameOver2: cc.Node = null;

    @property(cc.Node)
    txtTimeGameOver2: cc.Node = null;

    //parameter
    public checkPause: boolean = false;

    public time;

    public pen: cc.Node;



    start() {
        GameController.instance = this;

        GameManager.score = GameManager.constScore;
        GameManager.countHeart = GameManager.constCountHeart;

        this.txtScore.getComponent(cc.Label).string = GameManager.score.toString();
        this.txtHeart.getComponent(cc.Label).string = GameManager.countHeart.toString();
        this.gameOver1.active = false;
        this.gameOver2.active = false;

        var node = cc.instantiate(this.prefabParent);
        node.parent = this.parent;
        this.parentVirus = node;
        node.setPosition(0, 0);

        this.countVirus = 1;
        this.OnClick_CreatePen()
        if (!GameManager.login) {
            this.OnClick_CreateVirusRandom();
        }

        this.time = new Date().getTime();

        function locationHashChanged() {
            if (!GameManager.login) {
                GameController.instance.HashChange();
            }
        }
        window.onhashchange = locationHashChanged;
    }
    HashChange() {
        if (location.hash === ConfigString.hashtag_rule) {
            GameController.instance.OnClick_Resume();
            GameController.instance.gameOver1.active = false;
            GameController.instance.gameOver2.active = false;

        }

        if (location.hash === ConfigString.hashtag_gift) {
            GameController.instance.OnClick_Resume();
            GameController.instance.gameOver1.active = false;
            GameController.instance.gameOver2.active = false;
        }

        if (location.hash === ConfigString.hashtag_pause) {
            GameController.instance.gameOver1.active = false;
            GameController.instance.gameOver2.active = false;
            GameController.instance.OnClick_Pause();
        }

        if (location.hash === ConfigString.hashtag_gameover1) {
            GameController.instance.OnClick_Resume();
            GameController.instance.gameOver1.active = true;
            GameController.instance.gameOver2.active = false;
        }

        if (location.hash === ConfigString.hashtag_gameover2) {

            GameController.instance.OnClick_Resume();
            GameController.instance.gameOver2.active = true;
            GameController.instance.gameOver1.active = false;
        }
    }

    Time() {
        var time = new Date().getTime();
        return time;
    }

    PlusScore(_score) {
        if (!GameManager.login) {
            if (_score != 0) {
                GameManager.score += _score;
                this.txtScore.getComponent(cc.Label).string = GameManager.score.toString();
                this.txtScore.getComponent(cc.Animation).play();
                CustomGameplay.instance.PlayAudioHitTrue();
            }
        }
    }

    ShowScoreSocket(_score, _beforeScore) {
        if (parseInt(_score) !== _beforeScore) {
            this.txtScore.getComponent(cc.Label).string = _score.toString();
            this.txtScore.getComponent(cc.Animation).play();
            CustomGameplay.instance.PlayAudioHitTrue();
        }
        SocketController.instance.score = parseInt(_score);
    }

    ShowHeartSocket(_heart, _beforeHeart) {
        if (parseInt(_heart) > _beforeHeart) {
            this.txtHeart.getComponent(cc.Label).string = _heart.toString();
            this.txtHeart.getComponent(cc.Animation).play();
            CustomGameplay.instance.PlayAudioHitTrue();
        }

        if (parseInt(_heart) < _beforeHeart) {
            this.txtHeart.getComponent(cc.Label).string = _heart.toString();
            this.txtHeart.getComponent(cc.Animation).play();
            CustomGameplay.instance.PlayAudioHitFalse();
        }
        SocketController.instance.heart = parseInt(_heart);
    }

    GetTurn() {
        this.txtTurnGameOver.getComponent(cc.Label).string = ConfigString.strBeforeCountPlay + GameManager.countPlay + ConfigString.strAfterCountPlay;
    }

    CheckGameOver(countHeart) {
        if (parseInt(countHeart) <= 0) {
            this.GameOver();
        }
    }

    PlusHeart(_heart) {
        if (!GameManager.login) {
            if (_heart != 0) {
                GameManager.countHeart += _heart;
                this.txtHeart.getComponent(cc.Label).string = GameManager.countHeart.toString();
                this.txtHeart.getComponent(cc.Animation).play();
                this.CheckGameOver(GameManager.countHeart);
                if (_heart > 0) {
                    CustomGameplay.instance.PlayAudioHitTrue();
                }
                else {
                    CustomGameplay.instance.PlayAudioHitFalse();
                }
            }
        }
    }


    MinusHeart() {
        this.PlusHeart
    }

    GameOver() {
        var time = parseInt(((this.Time() - this.time) / 1000).toString());
        var a = time / 60;
        var minute = parseInt(a.toString());
        var second = time - minute * 60;


        this.parentVirus.destroy();
        if (GameManager.countPlay > 0) {
            this.gameOver1.active = true;
            this.gameOver2.active = false;
            if (minute < 10 && second < 10) {
                this.txtTimeGameOver1.getComponent(cc.Label).string = "0" + minute + " : 0" + second;
            }
            if (minute < 10 && second >= 10) {
                this.txtTimeGameOver1.getComponent(cc.Label).string = "0" + minute + " : " + second;
            }
            if (minute >= 10 && second < 10) {
                this.txtTimeGameOver1.getComponent(cc.Label).string = minute + " : 0" + second;
            }
            if (minute >= 10 && second >= 10) {
                this.txtTimeGameOver1.getComponent(cc.Label).string = minute + " : " + second;
            }

            this.txtScoreGameOver1.getComponent(cc.Label).string = GameManager.score.toString();
            this.GetTurn();
        }
        else {
            this.gameOver1.active = false;
            this.gameOver2.active = true;
            this.txtTimeGameOver2.getComponent(cc.Label).string = minute + " : " + second;
            this.txtScoreGameOver2.getComponent(cc.Label).string = GameManager.score.toString();
        }

    }

    onLoad() {
        cc.director.getCollisionManager().enabled = true;
        cc.director.getCollisionManager().enabledDebugDraw = false;
        cc.director.getCollisionManager().enabledDrawBoundingBox = false;
        cc.director.getPhysicsManager().enabled = true;


    }

    OnClick_RePlay() {
        GameManager.countPlay--;
        cc.director.loadScene("Game");
    }

    OnClick_CreateVirusRandom() {
        this.countVirus--;
        if (this.countVirus == 0) {
            this.CreateVirus();
        }
    }

    GetGift() {
        if (this.arrGiftSocket.length == 0) {
            SocketController.instance.GetGift();
        }
    }

    OnClick_CreatePen() {
        var node = cc.instantiate(this.prefabPen);
        LoadJsonController.instance.loadImagePen(node, GameManager.dataCustom.sprite.game.srcImgArrow);
        node.parent = this.parent;
        node.setPosition(0, -789);
        this.pen = node;
    }

    OnClick_Pause() {
        this.checkPause = true;
        this.popupPause.active = true;
        cc.director.pause();
    }

    OnClick_Resume() {
        this.checkPause = false;
        this.popupPause.active = false;
        cc.director.resume();
    }

    OnClick_ClosePopupGameOver2() {
        cc.director.loadScene("Menu");
    }

    EventCreate() {
        if (GameManager.countHeart > 0) {
            setTimeout(() => {
                this.OnClick_CreatePen();
                if (!GameManager.login) {
                    this.OnClick_CreateVirusRandom();
                }
                else {
                    this.GetGift();
                }
            }, 1000);
        }

    }

    ParamesterVirus(object: cc.Node) {

        var idGift = Math.floor(Math.random() * 100);
        var arrPercentGift: Number[] = [];
        arrPercentGift.push(0);

        var xhr = cc.loader.getXMLHttpRequest();

        for (var i = 0; i < GameManager.dataCustom.parameter_game.listItem.length; i++) {
            var percent: any = 0;
            for (var j = 0; j <= i; j++) {
                percent += GameManager.dataCustom.parameter_game.listItem[j].ratio;
            }
            arrPercentGift.push(percent);
        }
        for (var i = 0; i < arrPercentGift.length; i++) {
            if (arrPercentGift[i] <= idGift && idGift < arrPercentGift[i + 1]) {
                object.getComponent(Virus1Controller).type = GameManager.dataCustom.parameter_game.listItem[i].typeGift;
                object.getComponent(Virus1Controller).countHeart = GameManager.dataCustom.parameter_game.listItem[i].heart;
                object.getComponent(Virus1Controller).score = GameManager.dataCustom.parameter_game.listItem[i].score;
                LoadJsonController.instance.LoadImgItem(object, GameManager.dataCustom.parameter_game.listItem[i].srcImage);
            }
        }
    }

    ParamesterVirusSocket(object: cc.Node, _id, _type, _spriteObj, _countHeart, _score) {
        object.getComponent(Virus1Controller).id = _id;
        object.getComponent(Virus1Controller).type = _type;
        LoadJsonController.instance.LoadImgItem(object, _spriteObj);
        object.getComponent(Virus1Controller).countHeart = _countHeart;
        object.getComponent(Virus1Controller).score = _score;
    }


    CreateVirus() {
        this.countVirus = 0;

        var idGift = Math.floor(Math.random() * 100);
        if (idGift < CustomGameplay.instance.ratioTwoGift) {
            this.countVirus = 2;

            var node = cc.instantiate(this.virus3);
            node.getComponent(cc.Sprite).spriteFrame = null;
            node.parent = this.parentVirus;
            node.setPosition(this.posVirus3.x, this.posVirus3.y);
            node.getComponent(Virus1Controller).posItem = 0;
            this.ParamesterVirus(node);

            var node1 = cc.instantiate(this.virus2);
            node1.getComponent(cc.Sprite).spriteFrame = null;
            node1.parent = this.parentVirus;
            node1.setPosition(this.posVirus2.x, this.posVirus2.y);
            node1.getComponent(Virus1Controller).posItem = 1;
            this.ParamesterVirus(node1);
        }

    }

    checkLive: boolean = false;

    CreateItem() {
        if (!this.checkLive) {
            if (!GameManager.login)
                this.CreateVirus();
            else {
                SocketController.instance.GetGift();
            }
            this.checkLive = true;
        }
        setTimeout(() => {
            this.checkLive = false;
        }, 500);
    }

    CreateVirusSocket(gift1, gift2) {
        this.arrGiftSocket = [];
        var node = cc.instantiate(this.virus3);
        node.getComponent(cc.Sprite).spriteFrame = null;
        node.parent = this.parentVirus;
        node.setPosition(this.posVirus3.x, this.posVirus3.y);
        this.ParamesterVirusSocket(node, gift1.id, gift1.typeGift, gift1.srcImage, gift1.heart, gift1.score);
        node.getComponent(Virus1Controller).posItem = 0;
        node.getComponent(Virus1Controller).time = 0.385;

        var node1 = cc.instantiate(this.virus2);
        node1.getComponent(cc.Sprite).spriteFrame = null;
        node1.parent = this.parentVirus;
        node1.setPosition(this.posVirus2.x, this.posVirus2.y);
        this.ParamesterVirusSocket(node1, gift2.id, gift2.typeGift, gift2.srcImage, gift2.heart, gift2.score);
        node1.getComponent(Virus1Controller).posItem = 1;
        node1.getComponent(Virus1Controller).time = 0.26;

        this.arrGiftSocket.push(node1);
        this.arrGiftSocket.push(node);
    }


    public enter: boolean = false;

    Shoot() {

        this.enter = false;

        var data = [];
        for (var i = 0; i < this.arrGiftSocket.length; i++) {
            var a = { id: "", pos: 0, posX: 0, cycle: 0, collideWidth: 0, statusMoveLeft: false };
            a.id = GameController.instance.arrGiftSocket[i].getComponent(Virus1Controller).id;
            a.pos = this.arrGiftSocket[i].getComponent(Virus1Controller).posItem;
            a.posX = this.arrGiftSocket[i].x;
            a.cycle = CustomGameplay.instance.cycle;
            a.collideWidth = this.arrGiftSocket[i].getComponent(cc.BoxCollider).size.width + 50;
            a.statusMoveLeft = this.arrGiftSocket[i].getComponent(Virus1Controller).statusMoveLeft;
            data.push(a);
        }

        SocketController.instance.Shoot(data);
        let legalHit = [];

        for (let i = 0; i < data.length; i++) {
            if (this.checkHit(data[i].pos, data[i].posX, data[i].cycle, data[i].collideWidth, data[i].statusMoveLeft)) {
                legalHit.push(data[i]);
            }
        }

        if (legalHit.length > 0) {
            this.Enter(legalHit[0].id);
        }
    }

    checkHit = (pos: string | number, posX: number, cycle: number, colliderWidth: number, statusMoveLeft: Boolean) => {
        if (pos == 0) {
            if (!statusMoveLeft) {
                if (Math.abs(posX - 900 / cycle * 0.26) < (colliderWidth / 2 + 20)) {
                    return true;
                }
            }
            else {
                if (Math.abs(posX + 900 / cycle * 0.26) < (colliderWidth / 2 + 20)) {
                    return true;
                }
            }

        } else {
            if (!statusMoveLeft) {
                if (Math.abs(posX - 900 / cycle * 0.385) < (colliderWidth / 2 + 20)) {
                    return true;
                }
            }
            else {
                if (Math.abs(posX + 900 / cycle * 0.385) < (colliderWidth / 2 + 20)) {
                    return true;
                }
            }

        }
        return false;
    }




    Enter(id) {

        for (var i = 0; i < this.arrGiftSocket.length; i++) {
            if (this.arrGiftSocket[i].getComponent(Virus1Controller).id == id) {
                this.pen.getComponent(PenController).Enter(this.arrGiftSocket[i], this.arrGiftSocket[i].getComponent(Virus1Controller).time * 1000);
                this.arrGiftSocket.splice(i, 1);
            }
        }
    }

}
