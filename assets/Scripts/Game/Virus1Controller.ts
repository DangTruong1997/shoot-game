import GameManager from "../Manager/GameManager";
import SocketController from "../Socket/SocketController";
import CustomGameplay from "./CustomGameplay";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameController from "./GameController";
import PenController from "./PenController";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Virus1Controller extends cc.Component {

    public id: string = "";

    public type: Number = 0;

    public countHeart: Number = 0;

    public score: Number = 0;

    public posItem: number;

    public time : number;

    @property
    posXMin: number = 0;

    @property
    posXMax: number = 0;

    tween: cc.Tween;

    countLive: number = 0;

    @property
    startMove: number = 0;
    nodeX = 305;

    nodeY = 289;

    statusLife: boolean = false;
    statusMoveLeft: boolean = false;

    create(spriteObj) {
        var x, y;
        if (spriteObj.getOriginalSize().width > spriteObj.getOriginalSize().height) {
            x = this.nodeX;
            var anpha = this.nodeX / spriteObj.getOriginalSize().width;
            y = anpha * spriteObj.getOriginalSize().height;
        }
        else {
            y = this.nodeY;
            var anpha = this.nodeY / spriteObj.getOriginalSize().height;
            x = anpha * spriteObj.getOriginalSize().width;
        }

        this.node.getComponent(cc.Sprite).spriteFrame = spriteObj;
        this.node.setContentSize(x, y);

        this.node.getComponent(cc.BoxCollider).size = new cc.Size(x, 40);
        this.node.getComponent(cc.BoxCollider).offset = new cc.Vec2(0, (-y/2)+20);
        if (this.startMove == 0) {
            this.MoveLeft();
        }
        else {
            this.MoveRight();
        }
    }

    checkDie: boolean = false;

    update() {
        if (this.checkDie) {
            this.node.rotation += 10;
        }
    }

    public ActionAnimation() {
        this.checkDie = true;
        this.node.getComponent(cc.Animation).play();
    }

    public stop() {
        this.tween.stop();
    }

    Pause() {
        this.tween.stop();

    }

    Resume() {
        this.tween.start();
    }

    CheckDie() {
        if (this.countLive < CustomGameplay.instance.countCreateNewAnim) {
            this.countLive++;
        }
        else {
            GameController.instance.CreateItem();

            this.DestroyVirus();

        }
    }

    MoveLeft() {
        this.statusMoveLeft = true;
        this.CheckDie();
        this.tween = cc.tween(this.node)
            .to(CustomGameplay.instance.cycle, { position: new cc.Vec3(this.posXMax, this.node.y, this.node.z) })
            .call(() => {
                this.MoveRight();
            })
            .start()
    }

    MoveRight() {
        this.statusMoveLeft = false;
        this.CheckDie();
        this.tween = cc.tween(this.node)
            .to(CustomGameplay.instance.cycle, { position: new cc.Vec3(this.posXMin, this.node.y, this.node.z) })
            .call(() => {
                this.MoveLeft();
            })
            .start()
    }

    DestroyVirus() {
        this.node.destroy();
    }
}
