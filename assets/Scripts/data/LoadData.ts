// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { url } from "inspector";
import ConfigString from "../ConfigString";
import GameManager from "../Manager/GameManager";
import SocketController from "../Socket/SocketController";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LoadData extends cc.Component {
    // @property
    url: string = "https://beta.botplus.io";
    namespace: string = "shootgame";
    repeatSocket = 2
    elmId: string = "604f3b90094aa0236813c96c"
    userId: string = "fakeID"
    start() {
        var param = this;
        if (!param.urlParam("preview")) {
            GameManager.login = true;
        }

        if (window.socketUrl) {
            param.url = window.socketUrl
        }
        if (window.query && window.query.userId) {
            param.userId = window.query.userId
        }
  
        let stringPayload = param.urlParam('payload')
        if (stringPayload) {
            try {
                let payload = JSON.parse(atob(stringPayload))
                if (payload.socketUrl) {
                    param.url = payload.socketUrl
                }
                if (payload.elmId) {
                    param.elmId = payload.elmId
                }
                if (payload.userId) {
                    param.userId = payload.userId
                }
                if (payload.preview) {
                    GameManager.login = false;
                }
            } catch (error) {
                console.error(error)
            }
        }

        GameManager.repeatSocket = param.repeatSocket;
        
        if (!cc.sys.isNative) {
            GameManager.socket = window.io(`${param.url}/${param.namespace}`, {
                transports: ['websocket', 'polling'],
                pingTimeout: 30000,
                reconnectionAttempts: 3,
                reconnectionDelayMax: 2000,
                reconnectionDelay: 1000,
                forceNew: true,
                query: {
                    userId: param.userId
                }
            });
            GameManager.socket.on("connect", function () {
            });

            GameManager.socket.emit('init-data', param.elmId, function (err, data) {
                console.log('init-data');

                if (err) {
                    //
                    if (err == "not-enough-turn") {
                        ///
                    }
                    else if (err == "play-failed") {

                    }
                }
                GameManager.dataCustom = data.gameConfig;
                GameManager.dataPlayer = data.userData;
                if (GameManager.login) {
                    GameManager.countPlay = parseInt(GameManager.dataPlayer.turnNumber);
                }
                else {
                    GameManager.countPlay = parseInt(GameManager.dataCustom.config_string.countPlay);
                }
                GameManager.url = GameManager.dataCustom.config_string.url;

                ConfigString.strBeforeCountPlay = GameManager.dataCustom.config_content_popup.strBeforeCountPlay;
                ConfigString.strAfterCountPlay = GameManager.dataCustom.config_content_popup.strAfterCountPlay;
                ConfigString.strTitleTheLe = GameManager.dataCustom.config_content_popup.strTitleTheLe;
                ConfigString.strKeyScore = GameManager.dataCustom.config_content_popup.strKeyScore;
                ConfigString.strKeytime = GameManager.dataCustom.config_content_popup.strKeytime;
                ConfigString.strTitlePhanThuong = GameManager.dataCustom.config_content_popup.strTitlePhanThuong;
                ConfigString.strTheLe = GameManager.dataCustom.config_content_popup.strTheLe;
                ConfigString.strPhanThuong = GameManager.dataCustom.config_content_popup.strPhanThuong;

                ConfigString.hashtag_rule = GameManager.dataCustom.hashtag.rule;
                ConfigString.hashtag_gift = GameManager.dataCustom.hashtag.gift;
                ConfigString.hashtag_pause = GameManager.dataCustom.hashtag.pause;
                ConfigString.hashtag_resume = GameManager.dataCustom.hashtag.resume;
                ConfigString.hashtag_gameover1 = GameManager.dataCustom.hashtag.gameover1;
                ConfigString.hashtag_gameover2 = GameManager.dataCustom.hashtag.gameover2;

                cc.director.loadScene("Menu");
            });

        }
    }
    urlParam(name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.search);
        return (results !== null && results[1]) ? results[1] : null;
    }
}
