// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class ConfigString extends cc.Component {

    public static strBeforeCountPlay = "";

    public static strAfterCountPlay = "";

    public static strTitleTheLe = "";

    public static strTitlePhanThuong = "";

    public static strTheLe = "";

    public static strPhanThuong = "";

    public static strKeyScore = "";

    public static strKeytime = "";

    public static hashtag_rule ="";

    public static hashtag_gift ="";

    public static hashtag_pause ="";
    
    public static hashtag_resume ="";

    public static hashtag_gameover1 ="";

    public static hashtag_gameover2 ="";

}
