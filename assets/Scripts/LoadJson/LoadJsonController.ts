// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Virus1Controller from "../Game/Virus1Controller";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LoadJsonController extends cc.Component {

    public static instance: LoadJsonController;

    onLoad() {
        LoadJsonController.instance = this;
    }

    async loadSound(nodeSound: cc.AudioSource, url) {
        cc.loader.load(url, (err, audioClip) => {
            this.ChangeSound(nodeSound, audioClip);
        });
    }

    ChangeSound(AudioSource: cc.AudioSource, AudioClip: cc.AudioClip) {
        AudioSource.clip = AudioClip;
        AudioSource.play();
    }

    LoadImg(_sprite: cc.Node, url) {
        this.loadImage(_sprite, url)
    }

    LoadImgItem(_sprite: cc.Node, url) {
        this.loadImageItem(_sprite, url)
    }


    LoadButton(_sprite: cc.Node, url) {
        this.loadButton(_sprite, url)
    }

    LoadText(_lable: cc.Label, url) {
        this.loadText(_lable, url)
    }

    async loadText(_lable: cc.Label, url) {
        _lable.string = url;
    }

    async loadImage(_image: cc.Node, url) {
        cc.loader.load(url, async function (err, texture) {
            let image = new cc.SpriteFrame(texture);
            var x = _image.getContentSize().width;
            var y = _image.getContentSize().height;


            var x = image.getOriginalSize().width;
            var y = image.getOriginalSize().height;

            var widthImage;
            var heightImage;
            if (x > y) {
                var hs = x / _image.getContentSize().width;
                widthImage = _image.getContentSize().width;
                heightImage = y / hs;
            }
            else {
                var hs = y / _image.getContentSize().height;
                heightImage = _image.getContentSize().height;
                widthImage = x / hs;
            }

            if (image != null)
                _image.getComponent(cc.Sprite).spriteFrame = image;

            _image.setContentSize(widthImage, heightImage);
            setTimeout(() => {
                _image.setContentSize(widthImage, heightImage);
            }, 200);

        });
    }

    async loadImagePen(_image: cc.Node, url) {
        cc.loader.load(url, async function (err, texture) {
            let image = new cc.SpriteFrame(texture);
            _image.getComponent(cc.Sprite).spriteFrame = image;
        });
    }

    async loadImageItem(_image: cc.Node, url) {
        cc.loader.load(url, async function (err, texture) {
            let image = new cc.SpriteFrame(texture);
            _image.getComponent(Virus1Controller).create(image);
        });
    }

    async loadButton(_button: cc.Node, url) {
        cc.loader.load(url, async function (err, texture) {

            let image = new cc.SpriteFrame(texture);
            _button.getComponent(cc.Button).normalSprite = image;
            _button.getComponent(cc.Button).pressedSprite = image;
            _button.getComponent(cc.Button).hoverSprite = image;
            _button.getComponent(cc.Button).disabledSprite = image;

            var x = image.getOriginalSize().width;
            var y = image.getOriginalSize().height;

            var widthBtn;
            var heightBtn;
            if (x > y) {
                var hs = x / _button.getContentSize().width;
                widthBtn = _button.getContentSize().width;
                heightBtn = y / hs;
            }
            else {
                var hs = y / _button.getContentSize().height;
                heightBtn = _button.getContentSize().height;
                widthBtn = x / hs;

                // heightBtn = _button.getContentSize().height;
                // widthBtn = (_button.getContentSize().width * y) / _button.getContentSize().height;
            }

            _button.children[0].getComponent(cc.Sprite).spriteFrame = image;

            _button.setContentSize(widthBtn, widthBtn);
            _button.children[0].setContentSize(widthBtn, heightBtn)

            setTimeout(() => {
                _button.setContentSize(widthBtn, heightBtn);
                _button.children[0].setContentSize(widthBtn, heightBtn)
            }, 200);
        });
    }
}
