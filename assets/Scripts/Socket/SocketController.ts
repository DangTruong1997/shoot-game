// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import socket from "../../../folder server/wm_phitieu/dist/socket";
import GameController from "../Game/GameController";
import GameManager from "../Manager/GameManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SocketController extends cc.Component {

    public static instance: SocketController;

    public global;

    score ;
    heart;

    start() {
        SocketController.instance = this;

        if (GameManager.login) {
            if (!cc.sys.isNative) {

                GameManager.socket.on("disconnect", function () {
                    clearInterval(SocketController.instance.global);
                });

                GameManager.socket.on("reconnect-failed", function () {
                    clearInterval(SocketController.instance.global);
                });

                GameManager.socket.emit('play', ",", function (err, data) {
                    if (err) {
                        //
                        if (err == "not-enough-turn") {
                            ///
                        }
                        else if (err == "play-failed") {

                        }
                    }
                    if (SocketController.instance.global == null) {
                        SocketController.instance.global = setInterval(function () {
                            GameManager.socket.emit("heartbeat");
                        }, GameManager.repeatSocket * 1000)
                    }

                    SocketController.instance.score = data.point;
                    SocketController.instance.heart = data.heart;
                    GameManager.countHeart = data.heart;
                    GameManager.constScore = data.point;
                    GameController.instance.CreateVirusSocket(data.gifts[0], data.gifts[1]);
                });
            }

        }
    }

    Shoot(data) {

        GameManager.socket.emit('throw-arrow', data, function (err, data) {
            if (err) {
                //
                if (err == "not-enough-turn") {
                }
                else if (err == "play-failed") {
                }
            }

            setTimeout(() => {
                GameManager.countHeart = parseInt(data.data.heart);
                GameManager.score = parseInt(data.data.point);

                GameController.instance.ShowHeartSocket(data.data.heart,SocketController.instance.heart);
                GameController.instance.ShowScoreSocket(data.data.point,SocketController.instance.score);

                GameController.instance.CheckGameOver(GameManager.countHeart);
            }, 0.25 * 1000);

        });
    }

    GetGift() {
        GameManager.socket.emit('generate-gift', ",", function (err, data) {
            if (err) {
                //
                if (err == "not-enough-turn") {
                    ///
                }
                else if (err == "play-failed") {
                }
            }
            GameController.instance.CreateVirusSocket(data[0], data[1]);
        });
    }
}
