// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameManager {

    public static constCountHeart: number = 3;

    public static constScore: number = 0;

    public static speedMove: number = 50;

    public static countHeart: number = 0;

    public static score: number = 0;

    public static countPlay: number = 1000;

    public static data : any = {};

    public static dataCustom : any = {}

    public static dataPlayer : any = {}

    public static url : string = ""

    public static socket : any;

    public static repeatSocket : number = 0;

    //Login
    public static login : boolean = false;

}
